/**
 * @format
 */
import React from "react";
import { AppRegistry } from "react-native";
import { name as appName } from "./app.json";
import { Provider } from "react-redux";
import RootNavigation from "./src/navigation/navigation-stack";
import AppStore from "./src/store";
require("react-native").unstable_enableLogBox();
//require("./src/utilities/disable-logs");

const App = () => (
  <Provider store={AppStore}>
    <RootNavigation />
  </Provider>
);

//Registering app with project
AppRegistry.registerComponent(appName, () => App);
