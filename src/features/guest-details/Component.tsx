import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, KeyboardAvoidingView, Platform } from 'react-native';
import { selectContactPhone } from 'react-native-select-contact';
import PropTypes from 'prop-types';
import DateTimePicker from '@react-native-community/datetimepicker';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import Icon from 'react-native-vector-icons/MaterialIcons';
import translate from '../../utilities/i18n';
import { NavigateToSplash } from '../../navigation/utilities/navigation-provider';
import BoxView from '../../modules/components/BoxView';
import TextView from '../../modules/components/TextView';
import InputTextView from '../../modules/components/InputTextView';
import ButtonItemView from '../../modules/components/ButtonItemView';
import { colorAccent, colorInfo, colorBlack, colorDanger } from '../../assets/styles';
import ButtonView from '../../modules/components/ButtonView';
import ModalView from '../../modules/components/ModalView';

import { IState, IProps } from './interfaces';
import { IPropsToState } from './interfaces';
import { getParam } from '../../navigation/utilities/navigator';

const initialState: IState = {
  transactionAccountDetails: null,
  upiAccountId: 'TEST',
  bankAccountId: 'TEST',
  showInvitationConfirmation: false,
  showCalendarConfirmation: false,
  transactionAccountId: '',
  guestName: '',
  guestContact: '',
  isGuestSelected: false,
  guestAmount: '',
  guestAmountValidTill: new Date(),
  accountHolderName: 'Ankit Jain',
  accountHolderUPI: 'Ankit@UPI',
  accountHolderBankAccountName: 'Axis Bank',
  accountHolderBankAccountNumber: 'XXXX-XXXX-XX67',
  accountHolderContact: '+91-9977092265'
};

export default class GuestDetails extends Component<IProps, IState> {
  state = initialState;

  static propTypes = {
    onFetchTransactionAccountDetails: PropTypes.func
  };

  componentDidMount() {
    this._handleNavigationParams();
  }

  _handleNavigationParams = () => {
    const transactionAccountId = getParam(this.props, 'transactionAccountId');
    console.log('@@@@transactionAccountId:', transactionAccountId);
    if (!transactionAccountId || transactionAccountId === '') {
      Toast.show('Transaction account id is missing');
      return;
    }
    this.setState({ transactionAccountId }, () => {
      this._dispatchFetchTransactionAccountDetails();
    });
  };

  _dispatchFetchTransactionAccountDetails = () => {
    const { transactionAccountId } = this.state;
    const { onFetchTransactionAccountDetails } = this.props;
    if (!onFetchTransactionAccountDetails) {
      return;
    }
    onFetchTransactionAccountDetails({ transactionAccountId });
  };

  _handleCancelInvitation = () => {};

  static getDerivedStateFromProps(props: IProps, state: IState) {
    const { transactionAccountDetails } = props;
    let payload: IPropsToState = {};

    if (!!transactionAccountDetails && transactionAccountDetails !== state.transactionAccountDetails) {
      payload['transactionAccountDetails'] = transactionAccountDetails;
    }
    return payload;
  }

  _handleOpenEditGuestInvitation = () => {};

  render() {
    const { transactionAccountDetails } = this.state;
    if (!transactionAccountDetails) {
      return null;
    }

    const { endUserName, contactNumber, invitationDetails } = transactionAccountDetails;

    const validityMessage = `${moment(invitationDetails['invitationValidFrom']).format('LL')} - ${moment(
      invitationDetails['invitationValidTill']
    ).format('LL')}`;

    return (
      <BoxView flex="one" background="primary">
        <KeyboardAvoidingView
          style={styles.keyboardView}
          behavior="padding"
          enabled
          keyboardVerticalOffset={60}
        >
          <ScrollView>
            <BoxView direction="row" padding="md" justify="spaceBetween" alignItems="center">
              <BoxView direction="row" alignItems="center">
                <TextView size="xxlg" label={'My Guest Details'} weight="bold" />
              </BoxView>
            </BoxView>
            <BoxView margin="md" elevation="sm" background="white" padding="md">
              <BoxView direction="row" alignItems="center" padding="ysm">
                <Icon name="info" size={26} />
                <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
                  <BoxView margin="xmd">
                    <TextView label={`${endUserName} (Guest Account)`} size="md" weight="bold" />
                    <TextView label={contactNumber} size="sm" />
                  </BoxView>
                </BoxView>
              </BoxView>

              <BoxView direction="row" alignItems="center" padding="ysm">
                <Icon name="info" size={26} />
                <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
                  <BoxView margin="xmd">
                    <TextView
                      label={`${invitationDetails['invitationByName']} (UPI Account Holder)`}
                      size="md"
                      weight="bold"
                    />
                    <TextView label={`${invitationDetails['invitationByContact']}`} size="sm" />
                  </BoxView>
                </BoxView>
              </BoxView>

              <BoxView direction="row" alignItems="center" padding="ysm">
                <Icon name="info" size={26} />
                <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
                  <BoxView margin="xmd">
                    <TextView label={'Elligible Amount'} size="md" weight="bold" />
                    <TextView label={`₹ ${invitationDetails['invitationElligibleAmount']}`} size="sm" />
                  </BoxView>
                </BoxView>
              </BoxView>

              <BoxView direction="row" alignItems="center" padding="ysm">
                <Icon name="date-range" size={26} />
                <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
                  <BoxView margin="xmd">
                    <TextView label={'Account Validity'} size="md" weight="bold" />
                    <TextView label={validityMessage} size="sm" />
                  </BoxView>
                </BoxView>
              </BoxView>
              <BoxView border="top" margin="ysm" padding="ymd">
                <BoxView margin="yxs">
                  <ButtonView style="info" label="Edit Account" />
                </BoxView>
                <BoxView margin="yxs">
                  <ButtonView style="danger" label="Close Account" />
                </BoxView>
              </BoxView>
              <BoxView>
                <BoxView margin="ymd" padding="md" background="lightGrey" border="xs" round="xs">
                  <TextView lines={2} label={`My Guest Transaction`} size="sm" weight="bold" />
                </BoxView>
                <BoxView></BoxView>
              </BoxView>
            </BoxView>
          </ScrollView>
        </KeyboardAvoidingView>
      </BoxView>
    );
  }
}

const styles = StyleSheet.create({
  keyboardView: { flex: 1, flexDirection: 'column', justifyContent: 'center' }
});
