import { connect } from 'react-redux';
import Component from './Component';
import { Dispatch } from 'redux';
import { IFetchTransactionAccountDetails } from './interfaces';
import { fetchTransactionAccountDetailsAction } from './action';

const mapStateToProps = (state: any) => {
  return {
    transactionAccountDetails: state.FetchTransactionAccountDetailsReducer.items,
    isFetchingTransactionAccountDetails: state.FetchTransactionAccountDetailsReducer.isFetching
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onFetchTransactionAccountDetails: (params: IFetchTransactionAccountDetails) => {
      dispatch(fetchTransactionAccountDetailsAction(params));
    }
  };
};

const GuestDetailsScreen = connect(mapStateToProps, mapDispatchToProps)(Component);
export default GuestDetailsScreen;
