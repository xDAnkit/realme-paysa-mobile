export interface ICreateInvitationRequestParams {
  upiAccountId: string;
  guestName: string;
  guestContact: string;
  guestAmount: string;
  guestAmountValidity: string;
}

export interface ICreateInvitationRequest {
  type: string;
  params: ICreateInvitationRequestParams;
}

export interface IFetchTransactionAccountDetails {
  transactionAccountId: string;
}

export interface IFetchTransactionAccountDetailsRequest {
  transactionAccountId: string;
}

interface IDeviceDetails {
  deviceId: string;
  deviceBrand: string;
  deviceManuacturer: string;
  deviceMacAddress: string;
  platform: string;
  osName: string;
  osVersion: string;
  osArchitecture: string;
  packageName: string;
  isDeviceRooted: boolean;
  isPinOrFingerprintSet: boolean;
  isEmulator: boolean;
}

export interface ICreateInvitation {
  upiAccountId: string;
  bankAccountId: string;
  guestName: string;
  guestContact: string;
  guestAmount: string;
  guestAmountValidTill: Date;
}

export interface IUpdateInvitation {
  recordId: string;
  upiAccountId?: string;
  bankAccountId?: string;
  guestAmount: string;
  guestAmountValidTill: Date;
}

interface IAuthenticationSuccessfulResponse {
  appDeviceId: string;
  memberToken: string;
  refreshToken: string;
}

export interface IAuthSuccessResponse {
  items: IAuthenticationSuccessfulResponse;
  isFetching: boolean;
}

export interface IAuthRequest {
  type: string;
  payload: ICreateInvitation;
}

export interface ICreatedInvitationResponse {
  transactionAccountId: string;
}

export interface IProps {
  onFetchTransactionAccountDetails?: (params: IFetchTransactionAccountDetailsRequest) => void;
  transactionAccountDetails: ITransactionAccount;
}

export interface IState {
  transactionAccountDetails: ITransactionAccount | null;
  upiAccountId: string;
  bankAccountId: string;
  showInvitationConfirmation: boolean;
  showCalendarConfirmation: boolean;
  transactionAccountId: string;
  guestName: string;
  guestContact: string;
  isGuestSelected: boolean;
  guestAmount: string;
  guestAmountValidTill: Date;
  accountHolderName: string;
  accountHolderUPI: string;
  accountHolderBankAccountName: string;
  accountHolderBankAccountNumber: string;
  accountHolderContact: string;
}

export interface AutheticateAccountReducer {
  items: any;
  isFetching: boolean;
}
export interface RootState {
  AutheticateAccountReducer: AutheticateAccountReducer;
}

export interface IPropsToState {
  transactionAccountDetails?: ITransactionAccount;
}

export interface ITransactionAccount {
  transactionAccountId: string;
  accountType: string;
  endUserId: string;
  endUserName: string;
  contactNumber: string;
  invitationDetails: IInvitationDetails;
  isTransactionAccountActive: boolean;
  transactionAccountStatus: boolean;
  STATUS?: boolean;
}

export interface IInvitationDetails {
  isGuestAccount?: boolean;
  invitationElligibleAmount?: number;
  invitationBy?: string;
  invitationByName?: string;
  invitationByContact?: string;
  invitationType?: string; // Regular, Business
  invitationValidFrom?: Date;
  invitationValidTill?: Date;
  isInvitationActive?: boolean;
}

export interface ITransactionAccountResponse {
  isFetching: boolean | null;
  items: null | ITransactionAccount;
}

export interface ITransactionAccountAction {
  type: string;
  results: Object | ITransactionAccount;
}

export interface ITransactionAccountActionRequest {
  type: string;
  params: IFetchTransactionAccountDetails;
  results: Object | ITransactionAccount;
}
