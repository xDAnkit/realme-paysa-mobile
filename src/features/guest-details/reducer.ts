import {
  FETCH_TRANSACTION_ACCOUNT_DETAILS,
  FETCH_TRANSACTION_ACCOUNT_DETAILS_FAILED,
  FETCH_TRANSACTION_ACCOUNT_DETAILS_SUCCEEDED
} from './action';
import { ITransactionAccountResponse, ITransactionAccountAction } from './interfaces';

const intialState = {
  isFetching: null,
  items: null
};

const FetchTransactionAccountDetailsReducer = (
  invitation: ITransactionAccountResponse = intialState,
  action: ITransactionAccountAction
) => {
  switch (action.type) {
    case FETCH_TRANSACTION_ACCOUNT_DETAILS:
      return {
        isFetching: true
      };
    case FETCH_TRANSACTION_ACCOUNT_DETAILS_SUCCEEDED:
      return {
        isFetching: false,
        items: action.results
      };
    case FETCH_TRANSACTION_ACCOUNT_DETAILS_FAILED:
      return {
        isFetching: false,
        items: {}
      };
    default:
      return invitation;
  }
};

export default FetchTransactionAccountDetailsReducer;
