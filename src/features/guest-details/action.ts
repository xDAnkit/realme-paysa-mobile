import { IFetchTransactionAccountDetails, ITransactionAccount } from './interfaces';

export const FETCH_TRANSACTION_ACCOUNT_DETAILS = 'FETCH_TRANSACTION_ACCOUNT_DETAILS';
export const FETCH_TRANSACTION_ACCOUNT_DETAILS_SUCCEEDED = 'FETCH_TRANSACTION_ACCOUNT_DETAILS_SUCCEEDED';
export const FETCH_TRANSACTION_ACCOUNT_DETAILS_FAILED = 'FETCH_TRANSACTION_ACCOUNT_DETAILS_FAILED';

export const fetchTransactionAccountDetailsAction = (params: IFetchTransactionAccountDetails) => {
  return { type: FETCH_TRANSACTION_ACCOUNT_DETAILS, params };
};

export const fetchTransactionAccountDetailsSuccessAction = (results: ITransactionAccount) => {
  return { type: FETCH_TRANSACTION_ACCOUNT_DETAILS_SUCCEEDED, results };
};

export const fetchTransactionAccountDetailsFailedAction = () => {
  return { type: FETCH_TRANSACTION_ACCOUNT_DETAILS_FAILED };
};
