/**
 * Author: Ankit Jain
 * Organization: InOffice Labs
 * Project Name: InOffice
 * Date: 26-March-2020
 */

import { put, takeLatest } from 'redux-saga/effects';
import Toast from 'react-native-simple-toast';
import { GET, IHTTPDataResponse, SERVER_ERROR, INVITATION_API } from '../../utilities/network';
import {
  fetchTransactionAccountDetailsFailedAction,
  fetchTransactionAccountDetailsSuccessAction,
  FETCH_TRANSACTION_ACCOUNT_DETAILS
} from './action';
import { ITransactionAccountAction, ITransactionAccountActionRequest } from './interfaces';
import { TRANSACTION_ACCOUNT_URL } from '../../utilities/network/url-properties';

function* fetchTransactionAccountDetails(actions: ITransactionAccountActionRequest) {
  try {
    const URL = `${TRANSACTION_ACCOUNT_URL}/${actions['params']['transactionAccountId']}`;
    const response: IHTTPDataResponse = yield GET(URL, true);
    const { data, meta } = response;
    if (Object.keys(data).length === 0) {
      yield put(fetchTransactionAccountDetailsFailedAction());
      Toast.show(meta['message']);
      return;
    }
    yield put(fetchTransactionAccountDetailsSuccessAction(data['results']));
    return;
  } catch (error) {
    console.log('Error:', error);
    yield put(fetchTransactionAccountDetailsFailedAction());
    if (!error['response']) {
      Toast.show('This is a toast.');
      return;
    }
    if (error.response.status === SERVER_ERROR) {
      Toast.show('This is a toast.');
      return;
    }
  }
}

export function* watchFetchTransactionAccountDetails() {
  yield takeLatest(FETCH_TRANSACTION_ACCOUNT_DETAILS, fetchTransactionAccountDetails);
}
