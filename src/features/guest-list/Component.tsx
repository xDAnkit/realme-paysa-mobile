import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, KeyboardAvoidingView } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import translate from '../../utilities/i18n';
import BoxView from '../../modules/components/BoxView';
import TextView from '../../modules/components/TextView';
import { colorAccent, colorInfo, colorBlack } from '../../assets/styles';
import GuestItem from './components/GuestItem';
import ItemListView from '../../modules/components/ItemListView';
import ButtonItemView from '../../modules/components/ButtonItemView';
import { IProps, IState, IPropsToState } from './interfaces';
import { NavigateToGuestDetailsView } from '../../navigation/utilities/navigation-provider';

const initialState: IState = {
  page: 1,
  limit: 10,
  orderBy: 'updatedAt',
  records: {
    totalRecords: null,
    hasRecords: null,
    itemList: null
  }
};

export default class GuestList extends Component<IProps, IState> {
  state = initialState;

  static propTypes = {
    onFetchInvitedList: PropTypes.func
  };

  componentDidMount() {
    this._dispatchFetchInvitedGuestList();
  }

  _dispatchFetchInvitedGuestList = () => {
    const { page, limit, orderBy } = this.state;
    if (this.props.onFetchInvitedList) {
      this.props.onFetchInvitedList({ page, limit, orderBy });
    }
  };

  _handleOpenMakeTransactionView = (params: any) => {
    NavigateToGuestDetailsView(params);
  };

  static getDerivedStateFromProps(props: IProps, state: IState) {
    const { invitationList } = props;
    let payload: IPropsToState = {};

    if (!!invitationList && invitationList.response !== state.records.itemList) {
      payload['records'] = {
        totalRecords: invitationList.totalRecords,
        hasRecords: !!invitationList,
        itemList: invitationList.response
      };
    }
    return payload;
  }

  render() {
    const { records } = this.state;
    return (
      <BoxView flex="one" background="primary">
        <BoxView direction="row" padding="md" justify="spaceBetween" alignItems="center">
          <TextView size="xxlg" label={translate('GUEST_LIST_myGuestAccounts')} weight="bold" />
          <ButtonItemView>
            <Icon name="filter-list" size={42} color={colorBlack} />
          </ButtonItemView>
        </BoxView>
        <BoxView margin="md" elevation="sm" background="white" flex="one">
          <ItemListView
            click_1={this._handleOpenMakeTransactionView}
            component={GuestItem}
            records={records}
          />
        </BoxView>
      </BoxView>
    );
  }
}
