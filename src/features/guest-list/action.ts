import { IFetchGuestListRequest } from './interfaces';

export const FETCH_INVITED_GUEST = 'FETCH_INVITED_GUEST';
export const FETCH_INVITED_GUEST_SUCCEEDED = 'FETCH_INVITED_GUEST_SUCCEEDED';
export const FETCH_INVITED_GUEST_FAILED = 'FETCH_INVITED_GUEST_FAILED';

export const fetchInvitedGuestAction = (params: IFetchGuestListRequest) => {
  return { type: FETCH_INVITED_GUEST, params };
};

export const fetchInvitedGuestSuccessAction = (results: any) => {
  return { type: FETCH_INVITED_GUEST_SUCCEEDED, results };
};

export const fetchInvitedGuestFailedAction = () => {
  return { type: FETCH_INVITED_GUEST_FAILED };
};
