export interface IProps {
  onFetchInvitedList?: (params: IFetchGuestListRequest) => void;
  invitationList: IInvitationResponse | undefined | null;
  isFetchingInvitationList: boolean | null;
}

export interface IInvitationResponse {
  totalRecords: number | null | undefined;
  response: Array<IItems> | Array<{}> | null;
}

export interface IItems {
  recordId: string;
  endUserId: string;
  endUserName: string;
  contactNumber: string;
  accountType: string;
  invitationElligibleAmount: number;
  invitationType: string;
  invitationValidFrom: string;
  invitationValidTill: string;
  isInvitationActive: boolean;
  createdAt: string;
  updatedAt: string;
}

export interface IRecords {
  totalRecords: number | null | undefined;
  hasRecords: boolean | null;
  itemList: Array<IItems> | Array<{}> | null;
}

export interface IState {
  page: number;
  limit: number;
  orderBy: string;
  records: IRecords;
}

const initialState = {
  page: 1,
  limit: 10,
  orderBy: 'updatedAt',
  records: {
    totalRecords: null,
    hasRecords: null,
    itemList: null
  }
};

export interface IFetchGuestListRequest {
  page: number;
  limit: number;
  orderBy?: string;
}

export interface IFetchGuestListResponse {
  page: number;
  limit: number;
  orderBy?: string;
}

export interface IFetchListRequest {
  type: string;
  params: IFetchGuestListResponse;
}

export interface IPropsToState {
  records?: IRecords;
}

export interface IInvitedListResponse {
  totalRecords: number | undefined;
  response: IRecords;
}
