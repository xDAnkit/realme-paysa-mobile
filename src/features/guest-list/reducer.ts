import { FETCH_INVITED_GUEST, FETCH_INVITED_GUEST_SUCCEEDED, FETCH_INVITED_GUEST_FAILED } from './action';

const intialState = {
  isFetching: null,
  items: null
};

const FetchInvitedGuestReducer = (invitations: any = intialState, action: any) => {
  switch (action.type) {
    case FETCH_INVITED_GUEST:
      return {
        isFetching: true
      };
    case FETCH_INVITED_GUEST_SUCCEEDED:
      return {
        isFetching: false,
        items: action.results
      };
    case FETCH_INVITED_GUEST_FAILED:
      return {
        isFetching: false,
        items: []
      };
    default:
      return invitations;
  }
};

export default FetchInvitedGuestReducer;
