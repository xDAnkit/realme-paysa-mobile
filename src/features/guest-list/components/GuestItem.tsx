/**
 * @format
 */

import React from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import BoxView from '../../../modules/components/BoxView';
import TextView from '../../../modules/components/TextView';
import ButtonItemView from '../../../modules/components/ButtonItemView';
import { colorAccent, colorInfo, colorDanger } from '../../../assets/styles';
import ButtonView from '../../../modules/components/ButtonView';

const GuestItem: React.FC<GuestItemProps> = props => {
  const { item, click_1 } = props;
  const {
    recordId = '',
    endUserId = '',
    endUserName = '',
    contactNumber = '',
    accountType = '',
    invitationElligibleAmount,
    invitationType = '',
    invitationValidFrom = '',
    invitationValidTill = '',
    isInvitationActive = false,
    createdAt = '',
    updatedAt = ''
  } = item;

  const balanceMessage = `Allowed - ₹ ${invitationElligibleAmount}`;

  const statusMessage = !!isInvitationActive ? 'Active' : 'Inactive';
  const statusStyle = !!isInvitationActive ? 'success' : 'danger';

  return (
    <ButtonItemView
      onClick={() => {
        !!click_1 ? click_1(item) : null;
      }}
    >
      <BoxView direction="row" border="bottom" margin="ysm" padding="md" flex="one">
        <Icon name="account-circle" size={38} color={colorDanger} />
        <BoxView margin="xsm" justify="spaceBetween" flex="one">
          <TextView label={`${endUserName}`} size="lg" weight="bold" />
          <TextView label={balanceMessage} />
        </BoxView>
        <ButtonView style={statusStyle} label={statusMessage} size="xs" textSize="xs" />
      </BoxView>
    </ButtonItemView>
  );
};

interface IGuest {
  recordId: string;
  endUserId: string;
  endUserName: string;
  contactNumber: string;
  accountType: string;
  invitationElligibleAmount: number;
  invitationType: string;
  invitationValidFrom: string;
  invitationValidTill: string;
  isInvitationActive: boolean;
  createdAt: string;
  updatedAt: string;
}

interface GuestItemProps {
  item: IGuest;
  click_1?: (params: any) => void;
  click_2?: (params: any) => void;
  click_3?: (params: any) => void;
  click_4?: (params: any) => void;
  click_5?: (params: any) => void;
}

GuestItem.defaultProps = {
  item: {
    recordId: '',
    endUserId: '',
    endUserName: '',
    contactNumber: '',
    accountType: '',
    invitationElligibleAmount: 0,
    invitationType: '',
    invitationValidFrom: '',
    invitationValidTill: '',
    isInvitationActive: false,
    createdAt: '',
    updatedAt: ''
  },
  click_1: undefined,
  click_2: undefined,
  click_3: undefined,
  click_4: undefined,
  click_5: undefined
};

GuestItem.propTypes = {
  item: PropTypes.any,
  click_1: PropTypes.func || null,
  click_2: PropTypes.func || null,
  click_3: PropTypes.func || null,
  click_4: PropTypes.func || null,
  click_5: PropTypes.func || null
};

export default GuestItem;
