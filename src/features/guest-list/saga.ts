/**
 * Author: Ankit Jain
 * Organization: InOffice Labs
 * Project Name: InOffice
 * Date: 26-March-2020
 */

import { put, takeLatest } from 'redux-saga/effects';
import Toast from 'react-native-simple-toast';
import { GET, IHTTPDataResponse, SERVER_ERROR, INVITATION_API } from '../../utilities/network';
import { FETCH_INVITED_GUEST, fetchInvitedGuestFailedAction, fetchInvitedGuestSuccessAction } from './action';
import { IFetchListRequest } from './interfaces';

function* fetchInvitedGuestList(actions: IFetchListRequest) {
  try {
    const { page, limit, orderBy = 'updatedAt' } = actions['params'];
    let URL = `${INVITATION_API}?page=${page}&limit=${limit}&orderBy=${orderBy}`;
    const response: IHTTPDataResponse = yield GET(URL, true);
    const { data, meta } = response;
    if (Object.keys(data).length === 0) {
      yield put(fetchInvitedGuestFailedAction());
      Toast.show(meta['message']);
      return;
    }
    yield put(fetchInvitedGuestSuccessAction(data['results']));
    return;
  } catch (error) {
    console.log('Error:', error);
    yield put(fetchInvitedGuestFailedAction());
    if (!error['response']) {
      Toast.show('This is a toast.');
      return;
    }
    if (error.response.status === SERVER_ERROR) {
      Toast.show('This is a toast.');
      return;
    }
  }
}

export function* watchFetchInvitedGuestList() {
  yield takeLatest(FETCH_INVITED_GUEST, fetchInvitedGuestList);
}
