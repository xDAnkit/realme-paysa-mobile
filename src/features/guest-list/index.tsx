import { connect } from 'react-redux';
import Component from './Component';
import { Dispatch } from 'redux';
import { IFetchGuestListRequest } from './interfaces';
import { fetchInvitedGuestAction } from './action';

const mapStateToProps = (state: any) => {
  return {
    invitationList: state.FetchInvitedGuestReducer.items,
    isFetchingInvitationList: state.FetchInvitedGuestReducer.isFetching
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onFetchInvitedList: (params: IFetchGuestListRequest) => {
      dispatch(fetchInvitedGuestAction(params));
    }
  };
};

const HomeScreen = connect(mapStateToProps, mapDispatchToProps)(Component);
export default HomeScreen;
