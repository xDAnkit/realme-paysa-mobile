import { IActions } from './interfaces';

export const REFRESH_MEMBER_TOKEN = 'REFRESH_MEMBER_TOKEN';
export const REFRESH_MEMBER_TOKEN_SUCCEEDED = 'REFRESH_MEMBER_TOKEN_SUCCEEDED';
export const REFRESH_MEMBER_TOKEN_FAILED = 'REFRESH_MEMBER_TOKEN_FAILED';

export const refreshMemberTokenAction = (params: IActions) => {
  return { type: REFRESH_MEMBER_TOKEN, params };
};

export const refreshMemberTokenSuccessAction = (results: IActions) => {
  return { type: REFRESH_MEMBER_TOKEN_SUCCEEDED, results };
};

export const refreshMemberTokenFailedAction = (error: IActions) => {
  return { type: REFRESH_MEMBER_TOKEN_FAILED, results: error };
};
