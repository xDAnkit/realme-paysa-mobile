import { REFRESH_MEMBER_TOKEN, REFRESH_MEMBER_TOKEN_SUCCEEDED, REFRESH_MEMBER_TOKEN_FAILED } from './action';

const initialState = {
  items: null,
  isFetching: null
};
const RefreshMemberTokenReducers = (token = initialState, action: any) => {
  switch (action.type) {
    case REFRESH_MEMBER_TOKEN:
      return {
        isFetching: true
      };
    case REFRESH_MEMBER_TOKEN_SUCCEEDED:
      return {
        isFetching: false,
        items: action.results
      };
    case REFRESH_MEMBER_TOKEN_FAILED:
      return {
        items: {},
        isFetching: false
      };
    default:
      return token;
  }
};

export default RefreshMemberTokenReducers;
