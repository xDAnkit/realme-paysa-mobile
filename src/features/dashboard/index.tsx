import { connect } from 'react-redux';
import Component from './Component';
import { Dispatch } from 'redux';

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {};
};

const DashboardScreen = connect(mapStateToProps, mapDispatchToProps)(Component);
export default DashboardScreen;
