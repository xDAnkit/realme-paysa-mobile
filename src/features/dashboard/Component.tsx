import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import translate from "../../utilities/i18n";
import BoxView from "../../modules/components/BoxView";
import TextView from "../../modules/components/TextView";
import {
  colorAccent,
  colorInfo,
  colorBlack,
  colorSuccess,
  colorDanger
} from "../../assets/styles";
import ItemListView from "../../modules/components/ItemListView";
import ButtonItemView from "../../modules/components/ButtonItemView";
import {
  NavigateToCreateInvitation,
  NavigateToSentInvitationsList,
  NavigateToMakeTransactionView
} from "../../navigation/utilities/navigation-provider";

const initialState = {};

export default class Dashboard extends Component {
  state = initialState;

  _handleOpenInvitationView = () => {
    NavigateToCreateInvitation();
  };

  _handleOpenMySentInvitationsListView = () => {
    NavigateToSentInvitationsList();
  };

  _handleOpenMakeTransactionView = () => {
    NavigateToMakeTransactionView();
  };

  render() {
    return (
      <BoxView flex="one" background="primary">
        <BoxView padding="md" flex="two">
          <BoxView direction="row" justify="spaceBetween" alignItems="center">
            <BoxView>
              <TextView size="xxlg" label="Welcome Back" weight="bold" />
              <TextView size="xlg" label="Ankit Jain" />
            </BoxView>
            <ButtonItemView>
              <Icon name="account-circle" size={42} color={colorSuccess} />
            </ButtonItemView>
          </BoxView>
          <TextView
            color="secondary"
            lines={2}
            size="lg"
            label="What would you like to do today with sharedPaySa"
          />
        </BoxView>
        <BoxView flex="nine" margin="md" elevation="sm" background="white">
          <BoxView background="grey" padding="md">
            <TextView label="MY UPI OPTIONS" weight="bold" />
          </BoxView>
          <ScrollView>
            <BoxView flex="one">
              <ButtonItemView onClick={this._handleOpenInvitationView}>
                <BoxView direction="row" padding="md" border="bottom">
                  <Icon name="share" size={38} color={colorSuccess} />
                  <BoxView margin="xmd" flex="one">
                    <TextView
                      size="lg"
                      label="Invite for SharedPaySa"
                      weight="bold"
                    />
                    <TextView
                      size="xs"
                      label="Invite your Friends/Family/Business to use SharedPaySa."
                      color="secondary"
                    />
                  </BoxView>
                </BoxView>
              </ButtonItemView>
              <ButtonItemView
                onClick={this._handleOpenMySentInvitationsListView}
              >
                <BoxView direction="row" padding="md" border="bottom">
                  <Icon name="device-hub" size={38} color={colorSuccess} />
                  <BoxView margin="xmd" flex="one">
                    <TextView size="lg" label="My Invitations" weight="bold" />
                    <TextView
                      size="xs"
                      label="Invite your Friends/Family/Business to use SharedPaySa."
                      color="secondary"
                    />
                  </BoxView>
                </BoxView>
              </ButtonItemView>
              <ButtonItemView onClick={this._handleOpenMakeTransactionView}>
                <BoxView direction="row" padding="md" border="bottom">
                  <Icon
                    name="account-balance-wallet"
                    size={38}
                    color={colorSuccess}
                  />
                  <BoxView margin="xmd" flex="one">
                    <TextView size="lg" label="Pay Now" weight="bold" />
                    <TextView
                      size="xs"
                      label="Pay now using your UPI and SharedPaySa Account."
                      color="secondary"
                    />
                  </BoxView>
                </BoxView>
              </ButtonItemView>
              <ButtonItemView>
                <BoxView direction="row" padding="md" border="bottom">
                  <Icon name="https" size={38} color={colorSuccess} />
                  <BoxView margin="xmd" flex="one">
                    <TextView size="lg" label="Secure Lock-In" weight="bold" />
                    <TextView
                      size="xs"
                      label="Enable Third Step of Verification to secure account."
                      color="secondary"
                    />
                  </BoxView>
                </BoxView>
              </ButtonItemView>
            </BoxView>
          </ScrollView>
        </BoxView>
      </BoxView>
    );
  }
}
