import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import translate from "../../utilities/i18n";
import { NavigateToSplash } from "../../navigation/utilities/navigation-provider";
import BoxView from "../../modules/components/BoxView";
import TextView from "../../modules/components/TextView";

export default class Home extends Component {
  _handleNavigateToSplash = () => {
    NavigateToSplash();
  };

  render() {
    return (
      <BoxView background="danger" flex="one">
        <TextView
          label={`Lyghter Home ${translate("HOME_characters")}`}
          weight="bold"
        />
        <TouchableOpacity onPress={this._handleNavigateToSplash}>
          <TextView label="Click" />
        </TouchableOpacity>
      </BoxView>
    );
  }
}
