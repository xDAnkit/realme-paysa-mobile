import { connect } from 'react-redux';
import Component from './Component';
import { Dispatch } from 'redux';

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onFetchIFSCDetails: (params: any) => {
      dispatch(params);
    },
  };
};

const HomeScreen = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Component);
export default HomeScreen;
