import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, KeyboardAvoidingView } from 'react-native';
import LottieView from 'lottie-react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import translate from '../../utilities/i18n';
import { NavigateToSplash } from '../../navigation/utilities/navigation-provider';
import BoxView from '../../modules/components/BoxView';
import TextView from '../../modules/components/TextView';
import InputTextView from '../../modules/components/InputTextView';
import ButtonItemView from '../../modules/components/ButtonItemView';
import { colorAccent, colorInfo, colorSuccess } from '../../assets/styles';
import ButtonView from '../../modules/components/ButtonView';
import ModalView from '../../modules/components/ModalView';

const availableAccounts = [
  {
    transactionAccountId: 1,
    accountType: 'regular',
    upiId: 'UPI@ICICI',
    invitationDetails: {
      isInvitedAccount: true,
      accountHolderName: 'Ankit Jain',
      accountHolderUPI: 'ankit@icici',
      allowedBalance: 4000,
      isActive: true,
      validTill: new Date()
    }
  },
  {
    transactionAccountId: 2,
    accountType: 'regular',
    upiId: 'UPI@ICICI',
    invitationDetails: {
      isInvitedAccount: true,
      accountHolderName: 'Ankit Jain',
      accountHolderUPI: 'ankit@icici',
      allowedBalance: 4000,
      isActive: true,
      validTill: new Date()
    }
  },
  {
    transactionAccountId: 3,
    accountType: 'regular',
    upiId: 'UPI@ICICI',
    invitationDetails: {
      isInvitedAccount: true,
      accountHolderName: 'Ankit Jain',
      accountHolderUPI: 'ankit@icici',
      allowedBalance: 4000,
      isActive: true,
      validTill: new Date()
    }
  }
];
const initialState = {
  selectedAccount: {},
  availableAccounts,
  receiverUPI: '',
  receiverId: '',
  receiverType: '',
  receiverName: '',
  showAccountSelectionModal: false,
  showInvitationConfirmation: false,
  guestInvitationId: 'id',
  guestName: 'Neha Jain',
  guestContact: '+91-7898948331',
  guestAmount: '',
  guestAmountValidTill: '',
  accountHolderName: 'Ankit Jain',
  accountHolderUPI: 'Ankit@UPI',
  accountHolderBankAccountName: 'Axis Bank',
  accountHolderBankAccountNumber: 'XXXX-XXXX-XX67',
  accountHolderContact: '+91-9977092455'
};

export default class MakeTransaction extends Component {
  state = initialState;

  _renderSelectAccountModal = () => {
    return (
      <BoxView>
        <TextView label="ACCCOUNT LIST" />
      </BoxView>
    );
  };

  _renderConfirmDetailsModal = () => {
    const {
      guestName,
      guestContact,
      guestAmount,
      guestAmountValidTill,
      accountHolderName,
      accountHolderContact,
      accountHolderUPI,
      accountHolderBankAccountName,
      accountHolderBankAccountNumber
    } = this.state;

    const guestElligibleAmount = guestAmount === '' ? 0 : guestAmount;
    const guestElligibleAmountValidTill =
      guestAmountValidTill === '' ? 'Not Selected Yet' : guestAmountValidTill;
    return (
      <BoxView>
        <BoxView padding="ysm">
          <TextView label={'SENDING TO'} />
          <TextView
            size="lg"
            label={`${accountHolderName} -${accountHolderUPI} - (${accountHolderBankAccountName})`}
          />
        </BoxView>
        <BoxView padding="ysm">
          <TextView label={`Sending FROM`} />
          <TextView size="lg" label={`${guestName} (${guestContact} SharedPaySa Account)`} />
        </BoxView>
        <BoxView padding="ysm">
          <TextView label={`TRANSACTION AMOUNT`} />
          <TextView size="lg" label={`₹ ${guestElligibleAmount}/-`} />
        </BoxView>
        <BoxView padding="ysm" alignItems="center">
          <LottieView
            style={{ width: 140, height: 140 }}
            source={require('../../assets/anims/fingerprint.json')}
            autoPlay
            loop
          />
        </BoxView>

        <BoxView padding="ysm">
          <TextView
            weight="bold"
            align="center"
            label={`Please use your Biometric to authorize this Transaction`}
          />
        </BoxView>
      </BoxView>
    );
  };

  _toggleInvitationModal = () => {
    const { showInvitationConfirmation } = this.state;
    this.setState({ showInvitationConfirmation: !showInvitationConfirmation }, () => {
      if (!showInvitationConfirmation) {
        return;
      }
    });
  };

  _togglePaymentAccountModal = () => {
    const { showAccountSelectionModal } = this.state;
    this.setState({ showAccountSelectionModal: !showAccountSelectionModal }, () => {
      if (!showAccountSelectionModal) {
        return;
      }
    });
  };

  _handleCancelInvitation = () => {};

  _renderInvitationOptions = () => {
    const { guestInvitationId = '' } = this.state;

    if (guestInvitationId === '') {
      return (
        <BoxView margin="sm" direction="row" flex="one">
          <BoxView flex="one" margin="xs" elevation="sm">
            <ButtonView
              onClick={this._toggleInvitationModal}
              style="info"
              size="xxlg"
              label={`${translate('INVITATION_sendInvite')}`}
            />
          </BoxView>
        </BoxView>
      );
    }

    return (
      <BoxView margin="sm" direction="row" flex="one" justify="spaceBetween">
        <BoxView flex="five" margin="xs" elevation="sm">
          <ButtonView
            onClick={this._toggleInvitationModal}
            style="info"
            size="xxlg"
            label={`${translate('INVITATION_sendInvite')}`}
          />
        </BoxView>
        <BoxView flex="five" margin="xs" elevation="sm">
          <ButtonView style="danger" size="xxlg" label={`${translate('INVITATION_cancelInvite')}`} />
        </BoxView>
      </BoxView>
    );
  };

  _renderPayeeOption = () => {
    const {
      receiverUPI,
      receiverId,
      receiverName,
      receiverType,
      showInvitationConfirmation,
      guestInvitationId = '',
      guestName,
      guestContact,
      guestAmount,
      accountHolderName,
      accountHolderContact,
      accountHolderUPI,
      accountHolderBankAccountName,
      accountHolderBankAccountNumber
    } = this.state;

    if (receiverUPI === '') {
      return (
        <BoxView border="sm" margin="ymd" padding="lg" direction="row" justify="spaceBetween">
          <ButtonItemView>
            <BoxView alignItems="center">
              <Icon name="camera" size={56} color={colorInfo} />
              <TextView label="SCAN UPI QR" weight="bold" />
            </BoxView>
          </ButtonItemView>
          <ButtonItemView>
            <BoxView alignItems="center">
              <Icon name="keyboard" size={56} color={colorInfo} />
              <TextView label="ENTER UPI ID" weight="bold" />
            </BoxView>
          </ButtonItemView>
          <ButtonItemView>
            <BoxView alignItems="center">
              <Icon name="perm-contact-calendar" size={56} color={colorInfo} />
              <TextView label="OPEN CONTACT" weight="bold" />
            </BoxView>
          </ButtonItemView>
        </BoxView>
      );
    }
    return (
      <ButtonItemView>
        <BoxView direction="row" alignItems="center" padding="ymd">
          <Icon name="info" size={45} />
          <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
            <BoxView margin="xmd">
              <TextView label={accountHolderName} size="lg" weight="bold" />
              <TextView label={`${accountHolderUPI} - ${accountHolderBankAccountName}`} size="sm" />
            </BoxView>
            <Icon name="keyboard-arrow-right" size={26} color={colorAccent} />
          </BoxView>
        </BoxView>
      </ButtonItemView>
    );
  };

  render() {
    const {
      showInvitationConfirmation,
      showAccountSelectionModal,
      guestInvitationId = '',
      guestName,
      guestContact,
      guestAmount,
      accountHolderName,
      accountHolderContact,
      accountHolderUPI,
      accountHolderBankAccountName,
      accountHolderBankAccountNumber
    } = this.state;

    return (
      <BoxView flex="one" background="primary">
        <ModalView
          visible={showInvitationConfirmation}
          title="CONFIRM DETAILS WITH BIOMETRIC"
          close={this._toggleInvitationModal}
        >
          {this._renderConfirmDetailsModal()}
        </ModalView>
        <ModalView
          visible={showAccountSelectionModal}
          title="SELECT PAYMENT ACCOUNT"
          close={this._togglePaymentAccountModal}
        >
          {this._renderSelectAccountModal()}
        </ModalView>
        <KeyboardAvoidingView
          style={styles.keyboardView}
          behavior="padding"
          enabled
          keyboardVerticalOffset={60}
        >
          <ScrollView>
            <BoxView direction="row" padding="md" justify="spaceBetween" alignItems="center">
              <TextView size="xlg" label={'Pay using SharedPaySa'} weight="bold" />
              <Icon name="account-circle" size={42} color={colorInfo} />
            </BoxView>
            <BoxView margin="md" elevation="sm" background="white" padding="md">
              <ButtonItemView onClick={this._togglePaymentAccountModal}>
                <BoxView direction="row" alignItems="center" padding="ymd">
                  <Icon name="info" size={45} />
                  <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
                    <BoxView margin="xmd">
                      <TextView label={guestName} size="lg" weight="bold" />
                      <TextView label={`${guestContact} | SharedPaySa Account`} size="sm" />
                    </BoxView>
                    <Icon name="keyboard-arrow-right" size={26} color={colorAccent} />
                  </BoxView>
                </BoxView>
              </ButtonItemView>
              {this._renderPayeeOption()}

              <BoxView>
                <InputTextView
                  maxLength={5}
                  inputType="numeric"
                  size="xxxlg"
                  align="center"
                  value={guestAmount}
                  placeholder="₹ 0.0"
                  onChange={guestAmount => {
                    this.setState({ guestAmount });
                  }}
                />
              </BoxView>
              <BoxView padding="md" background="lightGrey" border="xs" round="xs">
                <TextView
                  lines={3}
                  label={`Note: Since You're sending money using realme SharedPaySa Account. Your parent account will be notified for this transaction.`}
                  size="sm"
                  weight="bold"
                />
              </BoxView>
            </BoxView>
            <BoxView margin="sm" direction="row" flex="one">
              <BoxView flex="one" margin="xs" elevation="sm">
                <ButtonView
                  onClick={this._toggleInvitationModal}
                  style="info"
                  size="xxlg"
                  label={`CONFIRM DETAILS`}
                />
              </BoxView>
            </BoxView>
          </ScrollView>
        </KeyboardAvoidingView>
      </BoxView>
    );
  }
}

const styles = StyleSheet.create({
  keyboardView: { flex: 1, flexDirection: 'column', justifyContent: 'center' }
});
