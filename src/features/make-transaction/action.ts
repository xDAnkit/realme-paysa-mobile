export const INIT_APP = 'INIT_APP';
export const INIT_APP_SUCCEEDED = 'INIT_APP_SUCCEEDED';
export const INIT_APP_FAILED = 'INIT_APP_FAILED';

export const initAppAction = (params: any) => {
  return { type: INIT_APP, params };
};

export const initAppSuccessAction = (results: any) => {
  return { type: INIT_APP_SUCCEEDED, results };
};

export const initAppFailedAction = (params: any) => {
  return { type: INIT_APP, params };
};
