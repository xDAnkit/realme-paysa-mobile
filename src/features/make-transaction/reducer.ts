import { INIT_APP, INIT_APP_SUCCEEDED, INIT_APP_FAILED } from './action';

const intialState = {
  isFetching: null,
  items: null,
};
const InitAppReducer = (appDevice: any = intialState, action: any) => {
  switch (action.type) {
    case INIT_APP:
      return {
        isFetching: true,
      };
    case INIT_APP_SUCCEEDED:
      return {
        isFetching: false,
        items: action.results,
      };
    case INIT_APP_FAILED:
      return {
        isFetching: false,
        items: {},
      };
    default:
      return appDevice; //No state changes
  }
};

export default InitAppReducer;
