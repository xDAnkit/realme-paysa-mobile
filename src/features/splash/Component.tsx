import React, { Component } from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import LottieView from "lottie-react-native";
import translate from "../../utilities/i18n";
import { NavigateToHome } from "../../navigation/utilities/navigation-provider";
import BoxView from "../../modules/components/BoxView";
import TextView from "../../modules/components/TextView";
import ButtonView from "../../modules/components/ButtonView";

export default class Splash extends Component {
  _handleNavigateToHome = () => {
    NavigateToHome();
  };

  render() {
    return (
      <BoxView flex="one" background="white">
        <BoxView flex="seven">
          <LottieView
            style={styles.loadAnimation}
            source={require("../../assets/anims/family.json")}
            autoPlay
            loop
          />
        </BoxView>
        <BoxView border="top" background="lightGrey" flex="five" padding="md">
          <TextView
            align="center"
            weight="bold"
            label={translate("SPLASH_welcomeToRealmeSharedPaysa")}
            size="xlg"
          />
          <TextView
            align="center"
            weight="bold"
            size="md"
            label={translate("SPLASH_realmeSharedPaysaTagline")}
          />

          <BoxView margin="ymd">
            <BoxView margin="ysm" elevation="sm">
              <ButtonView
                size="xlg"
                style="filled"
                label={translate("SPLASH_myGuestUPIAccount")}
              />
            </BoxView>
            <BoxView margin="ysm" elevation="sm">
              <ButtonView
                size="xlg"
                color="white"
                label={translate("SPLASH_needHelp")}
              />
            </BoxView>

            <BoxView margin="ysm" elevation="sm">
              <ButtonView
                size="xlg"
                style="filled"
                label={translate("SPLASH_myUPIAccount")}
              />
            </BoxView>
          </BoxView>
        </BoxView>
      </BoxView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },
  body: {
    justifyContent: "center",
    alignItems: "center"
  },
  customFont: {
    fontFamily: "quick sand"
  },
  button: {
    borderColor: "#E07800",
    backgroundColor: "#fff",
    borderWidth: 1.1,
    borderRadius: 10,
    marginTop: 42,
    padding: 12
  },
  buttonText: {
    fontFamily: "quicksand-Bold",
    color: "#E07800",
    fontSize: 14
  },
  mediumText: {
    fontFamily: "quicksand-Medium",
    textAlign: "center",
    fontSize: 24,
    color: "#34495e",
    marginTop: 16
  },
  normalText: {
    fontFamily: "quicksand-Medium",
    marginTop: 5,
    textAlign: "center",
    fontSize: 14,
    color: "#34495e"
  },
  brandIcon: {
    width: 90,
    height: 90
  },
  activityIndicatorStyle: {
    alignItems: "center",
    justifyContent: "center"
  },
  loadAnimation: {},
  centerLoader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "stretch"
  }
});
