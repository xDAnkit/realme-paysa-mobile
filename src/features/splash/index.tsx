import { connect } from 'react-redux';
import Splash from './Component';
import { Dispatch } from 'redux';

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onFetchIFSCDetails: (params: any) => {
      dispatch(params);
    },
  };
};

const SplashScreen = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Splash);
export default SplashScreen;
