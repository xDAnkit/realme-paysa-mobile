import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import translate from "../../utilities/i18n";
import { NavigateToSplash } from "../../navigation/utilities/navigation-provider";
import BoxView from "../../modules/components/BoxView";
import TextView from "../../modules/components/TextView";
import InputTextView from "../../modules/components/InputTextView";
import ButtonItemView from "../../modules/components/ButtonItemView";
import { colorAccent, colorInfo } from "../../assets/styles";
import ButtonView from "../../modules/components/ButtonView";
import ModalView from "../../modules/components/ModalView";

const initialState = {
  showInvitationConfirmation: false,
  guestInvitationId: "id",
  guestName: "Neha Jain",
  guestContact: "",
  guestAmount: "",
  guestAmountValidTill: "",
  accountHolderName: "Ankit Jain",
  accountHolderUPI: "Ankit@UPI",
  accountHolderBankAccountName: "Axis Bank",
  accountHolderBankAccountNumber: "XXXX-XXXX-XX67",
  accountHolderContact: "+91-9977092455"
};

export default class UPIPin extends Component {
  state = initialState;

  render() {
    const {
      showInvitationConfirmation,
      guestInvitationId = "",
      guestName,
      guestContact,
      guestAmount,
      accountHolderName,
      accountHolderContact,
      accountHolderUPI,
      accountHolderBankAccountName,
      accountHolderBankAccountNumber
    } = this.state;

    return (
      <BoxView flex="one" background="primary">
        <BoxView
          direction="row"
          padding="md"
          justify="spaceBetween"
          alignItems="center"
        >
          <TextView size="xxlg" label={"realme SharedPaySa"} weight="bold" />
          <Icon name="account-circle" size={42} color={colorInfo} />
        </BoxView>
        <BoxView margin="md" elevation="sm" background="white" padding="md">
          <BoxView>
            <InputTextView
              placeholder="Type your phone number here"
              value={guestContact}
              onChange={guestContact => {
                this.setState({ guestContact });
              }}
              inputType="numeric"
              maxLength={6}
              editable={false}
              border="xs"
              align="center"
              size="lg"
              background="white"
            />
          </BoxView>
          <BoxView direction="row" justify="spaceBetween" margin="md">
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="1"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="2"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="3"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
          </BoxView>

          <BoxView direction="row" justify="spaceBetween" margin="md">
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="4"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="5"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="6"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
          </BoxView>

          <BoxView direction="row" justify="spaceBetween" margin="md">
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="7"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="8"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="9"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
          </BoxView>

          <BoxView direction="row" justify="spaceBetween" margin="md">
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="*"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="0"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
            <BoxView flex="three">
              <ButtonView
                style="white"
                color="primary"
                label="#"
                size="lg"
                textSize="xxxlg"
              />
            </BoxView>
          </BoxView>
        </BoxView>
      </BoxView>
    );
  }
}
