import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import translate from "../../utilities/i18n";
import BoxView from "../../modules/components/BoxView";
import TextView from "../../modules/components/TextView";
import { colorAccent, colorInfo, colorBlack } from "../../assets/styles";
import GuestItem from "./components/GuestItem";
import ItemListView from "../../modules/components/ItemListView";
import ButtonItemView from "../../modules/components/ButtonItemView";
import InputTextView from "../../modules/components/InputTextView";

const initialState = {
  search: "",
  guestInvitationId: "123",
  guestName: "Neha",
  guestUPI: "Neha@icici",
  guestContact: "+91-9977092455",
  records: {
    totalRecords: 3,
    hasRecords: true,
    itemList: [
      {
        guestInvitationId: 1,
        accountId: "ACCOUNT_ID",
        tokenizationId: "TOKEN",
        transactionId: "123",
        transactionAmount: "200",
        transactionType: "merchant",
        recipientName: "Neon Cafe, Pune",
        recipientUPI: "neoncafe@icici",
        transactionLocation: [18.5843008, 73.7600338],
        transactionStatus: "Completed",
        createdAt: new Date()
      },
      {
        guestInvitationId: 2,
        accountId: "ACCOUNT_ID",
        tokenizationId: "TOKEN",
        transactionId: "124",
        transactionAmount: "900",
        transactionType: "non-merchant",
        recipientName: "Seema Jain",
        recipientUPI: "seema@icici",
        transactionLocation: [18.5843008, 73.7600338],
        transactionStatus: "Completed",
        createdAt: new Date()
      }
    ]
  }
};

export default class GuestTransactions extends Component {
  state = initialState;

  render() {
    const { records, guestName, guestContact, guestUPI, search } = this.state;

    return (
      <BoxView flex="one" background="primary">
        <BoxView
          direction="row"
          padding="md"
          justify="spaceBetween"
          alignItems="center"
        >
          <BoxView>
            <TextView
              size="xlg"
              label={`${guestName} 's ${translate(
                "GUEST_TRANSACTIONS_transactions"
              )}`}
              weight="bold"
            />

            <TextView label={`${guestContact} | ${guestUPI}`} />
          </BoxView>

          <ButtonItemView>
            <Icon name="filter-list" size={38} color={colorBlack} />
          </ButtonItemView>
        </BoxView>
        <BoxView margin="xmd">
          <InputTextView
            placeholder="Search Transaction"
            value={search}
            onChange={search => {
              this.setState({ search });
            }}
            leftIcon="search"
            elevation="sm"
            background="white"
          />
        </BoxView>
        <BoxView margin="md" elevation="sm" background="white" flex="one">
          <ItemListView component={GuestItem} records={records} />
        </BoxView>
      </BoxView>
    );
  }
}
