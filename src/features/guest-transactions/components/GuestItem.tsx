/**
 * @format
 */

import React from "react";
import moment from "moment";
import Icon from "react-native-vector-icons/MaterialIcons";
import PropTypes from "prop-types";
import BoxView from "../../../modules/components/BoxView";
import TextView from "../../../modules/components/TextView";
import ButtonItemView from "../../../modules/components/ButtonItemView";
import { colorDanger } from "../../../assets/styles";
import ButtonView from "../../../modules/components/ButtonView";

const GuestItem: React.FC<GuestItemProps> = props => {
  const { item } = props;
  const {
    transactionAmount = "",
    transactionType = "",
    recipientName = "",
    createdAt = "",
    recipientUPI = ""
  } = item;

  const transactionTimeStamp = moment(createdAt).fromNow();
  const transactionIcon =
    transactionType === "merchant" ? "store" : "perm-contact-calendar";
  return (
    <ButtonItemView>
      <BoxView
        direction="row"
        border="bottom"
        margin="ysm"
        padding="md"
        flex="one"
      >
        <Icon name={transactionIcon} size={38} color={colorDanger} />
        <BoxView margin="xsm" justify="spaceBetween" flex="one">
          <TextView label={recipientName} size="md" weight="bold" />
          <TextView
            size="xs"
            label={`${transactionTimeStamp} | ${recipientUPI}`}
          />
        </BoxView>
        <ButtonView
          style="success"
          label={`₹ ${transactionAmount}`}
          size="md"
          textSize="md"
        />
      </BoxView>
    </ButtonItemView>
  );
};

interface IGuest {
  guestInvitationId: string;
  accountId: string;
  tokenizationId: string;
  transactionId: string;
  transactionAmount: string;
  transactionType: string;
  recipientName: string;
  recipientUPI: string;
  transactionLocation: string;
  transactionStatus: string;
  guestAllowedBalance: string;
  createdAt: string;
}

interface GuestItemProps {
  item: IGuest;
  click_1?: () => void;
  click_2?: () => void;
  click_3?: () => void;
  click_4?: () => void;
  click_5?: () => void;
}

GuestItem.defaultProps = {
  item: {
    guestInvitationId: "",
    accountId: "",
    tokenizationId: "",
    transactionId: "",
    transactionAmount: "",
    transactionType: "",
    recipientName: "",
    recipientUPI: "",
    transactionLocation: "",
    transactionStatus: "",
    guestAllowedBalance: "",
    createdAt: ""
  },
  click_1: undefined,
  click_2: undefined,
  click_3: undefined,
  click_4: undefined,
  click_5: undefined
};

GuestItem.propTypes = {
  item: PropTypes.any,
  click_1: PropTypes.func || null,
  click_2: PropTypes.func || null,
  click_3: PropTypes.func || null,
  click_4: PropTypes.func || null,
  click_5: PropTypes.func || null
};

export default GuestItem;
