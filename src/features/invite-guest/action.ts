import { ICreatedInvitationResponse } from './interfaces';

export const CREATE_INVITATION = 'CREATE_INVITATION';
export const CREATE_INVITATION_SUCCEEDED = 'CREATE_INVITATION_SUCCEEDED';
export const CREATE_INVITATION_FAILED = 'CREATE_INVITATION_FAILED';

export const createInvitationAction = (params: any) => {
  return { type: CREATE_INVITATION, params };
};

export const createInvitationSuccessAction = (results: ICreatedInvitationResponse) => {
  console.log('@@@@results: ', results);
  return { type: CREATE_INVITATION_SUCCEEDED, results };
};

export const createInvitationFailedAction = () => {
  return { type: CREATE_INVITATION_FAILED };
};
