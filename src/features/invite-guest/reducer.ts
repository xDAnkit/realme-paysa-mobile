import { CREATE_INVITATION, CREATE_INVITATION_FAILED, CREATE_INVITATION_SUCCEEDED } from './action';

const intialState = {
  isFetching: null,
  items: null
};

const CreateInvitationReducer = (invitation: any = intialState, action: any) => {
  switch (action.type) {
    case CREATE_INVITATION:
      return {
        isFetching: true
      };
    case CREATE_INVITATION_SUCCEEDED:
      return {
        isFetching: false,
        items: action.results
      };
    case CREATE_INVITATION_FAILED:
      return {
        isFetching: false,
        items: {}
      };
    default:
      return invitation;
  }
};

export default CreateInvitationReducer;
