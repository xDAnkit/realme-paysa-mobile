import { connect } from 'react-redux';
import Component from './Component';
import { Dispatch } from 'redux';
import { ICreateInvitation } from './interfaces';
import { createInvitationAction } from './action';

const mapStateToProps = (state: any) => {
  return {
    invitatationResponse: state.CreateInvitationReducer.items,
    isFetchingInvitatationResponse: state.CreateInvitationReducer.isFetching
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onCreateInvitation: (params: ICreateInvitation) => {
      dispatch(createInvitationAction(params));
    }
  };
};

const InviteGuestScreen = connect(mapStateToProps, mapDispatchToProps)(Component);
export default InviteGuestScreen;
