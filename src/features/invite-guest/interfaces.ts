export interface ICreateInvitationRequestParams {
  upiAccountId: string;
  guestName: string;
  guestContact: string;
  guestAmount: string;
  guestAmountValidity: string;
}

export interface ICreateInvitationRequest {
  type: string;
  params: ICreateInvitationRequestParams;
}

interface IDeviceDetails {
  deviceId: string;
  deviceBrand: string;
  deviceManuacturer: string;
  deviceMacAddress: string;
  platform: string;
  osName: string;
  osVersion: string;
  osArchitecture: string;
  packageName: string;
  isDeviceRooted: boolean;
  isPinOrFingerprintSet: boolean;
  isEmulator: boolean;
}

export interface ICreateInvitation {
  upiAccountId: string;
  bankAccountId: string;
  guestName: string;
  guestContact: string;
  guestAmount: string;
  guestAmountValidTill: Date;
}

export interface IUpdateInvitation {
  recordId: string;
  upiAccountId?: string;
  bankAccountId?: string;
  guestAmount: string;
  guestAmountValidTill: Date;
}

interface IAuthenticationSuccessfulResponse {
  appDeviceId: string;
  memberToken: string;
  refreshToken: string;
}

export interface IAuthSuccessResponse {
  items: IAuthenticationSuccessfulResponse;
  isFetching: boolean;
}

export interface IAuthRequest {
  type: string;
  payload: ICreateInvitation;
}

export interface ICreatedInvitationResponse {
  transactionAccountId: string;
}

export interface IProps {
  onCreateInvitation?: (params: ICreateInvitation) => void;
  onUpdateInvitation?: (params: IUpdateInvitation) => void;
  invitatationResponse?: ICreatedInvitationResponse;
}

export interface IState {
  upiAccountId: string;
  bankAccountId: string;
  showInvitationConfirmation: boolean;
  showCalendarConfirmation: boolean;
  transactionAccountId: string;
  guestName: string;
  guestContact: string;
  isGuestSelected: boolean;
  guestAmount: string;
  guestAmountValidTill: Date;
  accountHolderName: string;
  accountHolderUPI: string;
  accountHolderBankAccountName: string;
  accountHolderBankAccountNumber: string;
  accountHolderContact: string;
}

export interface AutheticateAccountReducer {
  items: any;
  isFetching: boolean;
}
export interface RootState {
  AutheticateAccountReducer: AutheticateAccountReducer;
}

export interface IPropsToState {
  transactionAccountId?: string;
  showInvitationConfirmation?: boolean;
}
