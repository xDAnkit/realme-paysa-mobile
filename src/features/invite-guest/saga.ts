/**
 * Author: Ankit Jain
 * Organization: InOffice Labs
 * Project Name: InOffice
 * Date: 26-March-2020
 */

import { put, takeLatest } from 'redux-saga/effects';
import Toast from 'react-native-simple-toast';
import { POST, IHTTPDataResponse, SERVER_ERROR, INVITATION_API } from '../../utilities/network';
import { createInvitationSuccessAction, createInvitationFailedAction, CREATE_INVITATION } from './action';
import { ICreateInvitationRequest } from './interfaces';
import { NavigateToGuestDetailsView } from '../../navigation/utilities/navigation-provider';

function* createInvitation(actions: ICreateInvitationRequest) {
  try {
    const response: IHTTPDataResponse = yield POST(INVITATION_API, actions['params'], true);
    const { data, meta } = response;
    if (Object.keys(data).length === 0) {
      yield put(createInvitationFailedAction());
      Toast.show(meta['message']);
      return;
    }
    yield put(createInvitationSuccessAction(data['results']));
    NavigateToGuestDetailsView({ transactionAccountId: data['results']['transactionAccountId'] });
    Toast.show(meta['message']);
    return;
  } catch (error) {
    console.log('Error:', error);
    yield put(createInvitationFailedAction());
    if (!error['response']) {
      Toast.show('This is a toast.');
      return;
    }
    if (error.response.status === SERVER_ERROR) {
      Toast.show('This is a toast.');
      return;
    }
  }
}

export function* watchCreateInvitation() {
  yield takeLatest(CREATE_INVITATION, createInvitation);
}
