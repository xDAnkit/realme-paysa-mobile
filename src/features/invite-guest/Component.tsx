import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, KeyboardAvoidingView, Platform } from 'react-native';
import { selectContactPhone } from 'react-native-select-contact';
import PropTypes from 'prop-types';
import DateTimePicker from '@react-native-community/datetimepicker';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import Icon from 'react-native-vector-icons/MaterialIcons';
import translate from '../../utilities/i18n';
import { NavigateToSplash } from '../../navigation/utilities/navigation-provider';
import BoxView from '../../modules/components/BoxView';
import TextView from '../../modules/components/TextView';
import InputTextView from '../../modules/components/InputTextView';
import ButtonItemView from '../../modules/components/ButtonItemView';
import { colorAccent, colorInfo, colorBlack } from '../../assets/styles';
import ButtonView from '../../modules/components/ButtonView';
import ModalView from '../../modules/components/ModalView';

import { IState, IProps } from './interfaces';
import { IPropsToState } from './interfaces';

const initialState: IState = {
  upiAccountId: 'TEST',
  bankAccountId: 'TEST',
  showInvitationConfirmation: false,
  showCalendarConfirmation: false,
  transactionAccountId: '',
  guestName: '',
  guestContact: '',
  isGuestSelected: false,
  guestAmount: '',
  guestAmountValidTill: new Date(),
  accountHolderName: 'Ankit Jain',
  accountHolderUPI: 'Ankit@UPI',
  accountHolderBankAccountName: 'Axis Bank',
  accountHolderBankAccountNumber: 'XXXX-XXXX-XX67',
  accountHolderContact: '+91-9977092455'
};

export default class InviteGuest extends Component<IProps, IState> {
  state = initialState;

  static propTypes = {
    onCreateInvitation: PropTypes.func,
    onUpdateInvitation: PropTypes.func
  };

  getPhoneNumber = () => {
    return selectContactPhone().then(selection => {
      if (!selection) {
        return null;
      }

      let { contact, selectedPhone } = selection;
      const { name = '' } = contact;
      console.log('@@@@contact: ', contact);
      console.log('@@@@selectedPhone: ', selectedPhone);
      console.log(`Selected ${selectedPhone.type} phone number ${selectedPhone.number} from ${contact.name}`);

      const guestName = name === '' || name === ' ' ? 'My Guest' : name;
      this.setState({
        isGuestSelected: true,
        guestName,
        guestContact: selectedPhone.number
      });
      //return selectedPhone.number;
    });
  };

  _renderConfirmDetailsModal = () => {
    const {
      guestName,
      guestContact,
      guestAmount,
      guestAmountValidTill,
      accountHolderName,
      accountHolderContact,
      accountHolderUPI,
      accountHolderBankAccountName,
      accountHolderBankAccountNumber
    } = this.state;

    const guestElligibleAmount = guestAmount === '' ? 0 : guestAmount;
    return (
      <BoxView>
        <BoxView padding="ysm">
          <TextView label={`${translate('INVITATION_selectedAccount')}`} />
          <TextView
            size="lg"
            label={`${accountHolderName} -${accountHolderUPI} - (${accountHolderBankAccountName})`}
          />
        </BoxView>
        <BoxView padding="ysm">
          <TextView label={`${translate('INVITATION_guestNameAccount')}`} />
          <TextView size="lg" label={`${guestName} - (${guestContact})`} />
        </BoxView>
        <BoxView padding="ysm">
          <TextView label={`${translate('INVITATION_elligibleAmount')}`} />
          <TextView size="lg" label={`₹ ${guestElligibleAmount}/-`} />
        </BoxView>
        <BoxView padding="ysm">
          <TextView label={`${translate('INVITATION_amountValidity')}`} />
          <TextView size="lg" label={`Date: ${moment(guestAmountValidTill).format('LL')}`} />
        </BoxView>
        <BoxView padding="ysm">
          <ButtonView
            style="info"
            size="xxlg"
            label={'CONFIRM DETAILS'}
            onClick={this._handleConfirmInvitation}
          />
        </BoxView>
      </BoxView>
    );
  };
  _handleConfirmInvitation = () => {
    this.setState({ showInvitationConfirmation: false }, () => {
      this._dispatchCreateInvitation();
    });
  };

  _dispatchCreateInvitation = () => {
    const {
      upiAccountId,
      bankAccountId,
      guestName,
      guestContact,
      guestAmount,
      guestAmountValidTill
    } = this.state;

    if (this.props.onCreateInvitation) {
      this.props.onCreateInvitation({
        upiAccountId,
        bankAccountId,
        guestName,
        guestContact,
        guestAmount,
        guestAmountValidTill
      });
    }
  };

  _toggleInvitationModal = () => {
    const { showInvitationConfirmation, guestName, guestContact, guestAmount } = this.state;

    if (!showInvitationConfirmation && (guestName === '' || guestContact === '')) {
      Toast.show('Please select a contact first');
      return;
    }

    if (!showInvitationConfirmation && (guestAmount === '' || guestAmount == '0')) {
      Toast.show('Please enter a valid amount');
      return;
    }

    this.setState({ showInvitationConfirmation: !showInvitationConfirmation }, () => {
      if (!showInvitationConfirmation) {
        return;
      }
    });
  };

  _handleCancelInvitation = () => {};

  onDateChange = (event: Event, guestAmountValidTill: any) => {
    console.log(guestAmountValidTill);
    this.setState({
      guestAmountValidTill
    });
  };

  _toggleDatePicker = () => {
    if (Platform.OS === 'ios') {
      const { showCalendarConfirmation } = this.state;
      this.setState({ showCalendarConfirmation: !showCalendarConfirmation }, () => {
        if (!showCalendarConfirmation) {
          //Perform
          return;
        }
      });
      return;
    }
  };

  _setDate = () => {};

  _renderDatePicker = () => {
    const { guestAmountValidTill } = this.state;
    return (
      <BoxView>
        <DateTimePicker
          testID="dateTimePicker"
          timeZoneOffsetInMinutes={0}
          value={guestAmountValidTill}
          mode={'date'}
          is24Hour={true}
          display="default"
          onChange={this.onDateChange}
        />
        <ButtonView style="info" size="xxlg" label="CONFIRM" onClick={this._toggleDatePicker} />
      </BoxView>
    );
  };

  _renderInvitationOptions = () => {
    const { transactionAccountId = '', guestName, guestContact } = this.state;

    if (transactionAccountId !== '' && guestName !== '' && guestContact !== '') {
      return (
        <BoxView margin="sm" direction="row" flex="one" justify="spaceBetween">
          <BoxView flex="five" margin="xs" elevation="sm">
            <ButtonView
              onClick={this._toggleInvitationModal}
              style="info"
              size="xxlg"
              label={`${translate('INVITATION_sendInvite')}`}
            />
          </BoxView>
          <BoxView flex="five" margin="xs" elevation="sm">
            <ButtonView style="danger" size="xxlg" label={`${translate('INVITATION_cancelInvite')}`} />
          </BoxView>
        </BoxView>
      );
    }

    return (
      <BoxView margin="sm" direction="row" flex="one">
        <BoxView flex="one" margin="xs" elevation="sm">
          <ButtonView
            onClick={this._toggleInvitationModal}
            style="info"
            size="xxlg"
            label={`${translate('INVITATION_sendInvite')}`}
          />
        </BoxView>
      </BoxView>
    );
  };

  _renderGuestSelectionOption = () => {
    const { guestName, guestContact, transactionAccountId } = this.state;

    if (transactionAccountId !== '' && guestName !== '' && guestContact !== '') {
      return (
        <BoxView direction="row" alignItems="center" padding="ymd" justify="spaceBetween">
          <BoxView direction="row">
            <Icon name="info" size={45} />
            <BoxView margin="xmd">
              <TextView label={guestName} size="lg" weight="bold" />
              <TextView label={guestContact} size="sm" />
            </BoxView>
          </BoxView>
          <ButtonItemView>
            <Icon name="info" size={26} color={colorBlack} />
          </ButtonItemView>
        </BoxView>
      );
    }

    if (guestName === '' || guestContact === '') {
      return (
        <ButtonItemView onClick={this.getPhoneNumber}>
          <BoxView direction="row" alignItems="center" padding="ymd">
            <Icon name="info" size={45} />
            <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
              <BoxView margin="xmd">
                <TextView label={'Select a guest'} size="lg" weight="bold" color="secondary" />
                <TextView label={'Tap to select a guest from your contacts'} size="sm" color="secondary" />
              </BoxView>
              <Icon name="keyboard-arrow-right" size={26} color={colorBlack} />
            </BoxView>
          </BoxView>
        </ButtonItemView>
      );
    }

    return (
      <BoxView direction="row" alignItems="center" padding="ymd" justify="spaceBetween">
        <ButtonItemView onClick={this.getPhoneNumber}>
          <BoxView direction="row">
            <Icon name="info" size={45} />
            <BoxView margin="xmd">
              <TextView label={guestName} size="lg" weight="bold" />
              <TextView label={guestContact} size="sm" />
            </BoxView>
          </BoxView>
        </ButtonItemView>
        <ButtonItemView onClick={this._handleClearGuestDetails}>
          <Icon name="cancel" size={26} color={colorBlack} />
        </ButtonItemView>
      </BoxView>
    );
  };

  _handleClearGuestDetails = () => {
    this.setState({
      guestName: '',
      guestContact: ''
    });
  };

  static getDerivedStateFromProps(props: IProps, state: IState) {
    const { invitatationResponse } = props;
    let payload: IPropsToState = {};

    console.log('@@@@MJinvitatationResponse: ', invitatationResponse);

    if (!!invitatationResponse && invitatationResponse.transactionAccountId !== state.transactionAccountId) {
      //payload['transactionAccountId'] = invitatationResponse.transactionAccountId;
      //payload['showInvitationConfirmation'] = false;
    }
    return payload;
  }

  componentWillUnmount() {}

  render() {
    const {
      showInvitationConfirmation,
      showCalendarConfirmation,
      transactionAccountId,
      guestAmountValidTill,
      guestName,
      guestContact,
      guestAmount,
      accountHolderName,
      accountHolderContact,
      accountHolderUPI,
      accountHolderBankAccountName,
      accountHolderBankAccountNumber
    } = this.state;

    const formattedDate = moment(guestAmountValidTill).format('LL');
    const headerMessage =
      transactionAccountId === ''
        ? translate('INVITATION_createInvitation')
        : translate('INVITATION_editInvitation');
    return (
      <BoxView flex="one" background="primary">
        <ModalView
          visible={showInvitationConfirmation}
          title="VERIFY DETAILS"
          close={this._toggleInvitationModal}
        >
          {this._renderConfirmDetailsModal()}
        </ModalView>
        <ModalView visible={showCalendarConfirmation} title="SELECT DATE" close={this._toggleDatePicker}>
          {this._renderDatePicker()}
        </ModalView>
        <KeyboardAvoidingView
          style={styles.keyboardView}
          behavior="padding"
          enabled
          keyboardVerticalOffset={60}
        >
          <ScrollView>
            <BoxView direction="row" padding="md" justify="spaceBetween" alignItems="center">
              <TextView size="xxlg" label={headerMessage} weight="bold" />
              <Icon name="account-circle" size={42} color={colorInfo} />
            </BoxView>
            <BoxView margin="md" elevation="sm" background="white" padding="md">
              <ButtonItemView>
                <BoxView direction="row" alignItems="center" padding="ymd">
                  <Icon name="info" size={45} />
                  <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
                    <BoxView margin="xmd">
                      <TextView label={accountHolderName} size="lg" weight="bold" />
                      <TextView label={`${accountHolderUPI} - ${accountHolderBankAccountName}`} size="sm" />
                    </BoxView>
                    <Icon name="keyboard-arrow-right" size={26} color={colorBlack} />
                  </BoxView>
                </BoxView>
              </ButtonItemView>

              {this._renderGuestSelectionOption()}

              <ButtonItemView onClick={this._toggleDatePicker}>
                <BoxView direction="row" alignItems="center" padding="ymd">
                  <Icon name="date-range" size={45} />

                  <BoxView flex="one" direction="row" alignItems="center" justify="spaceBetween">
                    <BoxView margin="xmd">
                      <TextView label={`${translate('INVITATION_validTill')}`} size="lg" weight="bold" />
                      <TextView label={`${formattedDate}`} size="sm" />
                    </BoxView>
                    <Icon name="keyboard-arrow-right" size={26} color={colorBlack} />
                  </BoxView>
                </BoxView>
              </ButtonItemView>
              <BoxView>
                <InputTextView
                  maxLength={5}
                  inputType="numeric"
                  size="xxxlg"
                  align="center"
                  value={guestAmount}
                  placeholder="₹ 0.0"
                  onChange={guestAmount => {
                    this.setState({ guestAmount });
                  }}
                />
              </BoxView>
              <BoxView padding="md" background="lightGrey" border="xs" round="xs">
                <TextView
                  lines={2}
                  label={`${translate('INVITATION_youCanCancelAnytime')}`}
                  size="sm"
                  weight="bold"
                />
              </BoxView>
            </BoxView>
            {this._renderInvitationOptions()}
          </ScrollView>
        </KeyboardAvoidingView>
      </BoxView>
    );
  }
}

const styles = StyleSheet.create({
  keyboardView: { flex: 1, flexDirection: 'column', justifyContent: 'center' }
});
