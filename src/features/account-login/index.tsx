import { connect } from 'react-redux';
import Component from './Component';

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    onAuthLogin: (params: any) => {
      dispatch(params);
    }
  };
};

const HomeScreen = connect(mapStateToProps, mapDispatchToProps)(Component);
export default HomeScreen;
