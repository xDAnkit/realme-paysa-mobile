import { StyleSheet } from 'react-native';

const Styles = StyleSheet.create({
  animationIcon: { width: 130, height: 130 }
});

export default Styles;
