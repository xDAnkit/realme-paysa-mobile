/**
 * Author: Ankit Jain
 * Organization: InOffice Labs
 * Project Name: InOffice
 * Date: 26-March-2020
 */

import { put, takeLatest } from 'redux-saga/effects';
import Toast from 'react-native-simple-toast';
import { POST, IHTTPDataResponse, SERVER_ERROR } from '../../utilities/network';
import { authenticateAccountAction, authenticateAccountFailedAction, AUTHENTICATE_ACCOUNT } from './action';
import { IAuthRequest } from './interfaces';

function* authenticateAccount(actions: IAuthRequest) {
  try {
    const response: IHTTPDataResponse = yield POST('URL', actions['payload'], false);
    const { data, meta } = response;
    if (Object.keys(data).length === 0) {
      return;
    }
    yield put(authenticateAccountAction(data['results']));
    return;
  } catch (error) {
    yield put(authenticateAccountFailedAction());
    if (!error['response']) {
      Toast.show('This is a toast.');
      return;
    }
    if (error.response.status === SERVER_ERROR) {
      Toast.show('This is a toast.');
      return;
    }
  }
}

export function* watchAuthenticateAccount() {
  yield takeLatest(AUTHENTICATE_ACCOUNT, authenticateAccount);
}
