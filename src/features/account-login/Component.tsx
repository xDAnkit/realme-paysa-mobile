import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, KeyboardAvoidingView, Platform } from 'react-native';
import {
  getUniqueId,
  getManufacturer,
  getBrand,
  getMacAddress,
  getSystemName,
  getSystemVersion,
  getBundleId,
  isPinOrFingerprintSet,
  isEmulator,
  supportedAbis
} from 'react-native-device-info';
import PropTypes from 'prop-types';
import Toast from 'react-native-simple-toast';
import JailMonkey from 'jail-monkey';
import Icon from 'react-native-vector-icons/MaterialIcons';
import translate from '../../utilities/i18n';
import LottieView from 'lottie-react-native';
import BoxView from '../../modules/components/BoxView';
import TextView from '../../modules/components/TextView';
import InputTextView from '../../modules/components/InputTextView';
import ButtonItemView from '../../modules/components/ButtonItemView';
import { colorInfo } from '../../assets/styles';
import ButtonView from '../../modules/components/ButtonView';
import ModalView from '../../modules/components/ModalView';
import { IAccountLogin, IState, IProps } from './interfaces';
import Styles from './styles';

const initialState: IState = {
  verificationOTPCode: '',
  verificationOTPToken: '',
  showVerifyingDeviceModal: false,
  contactNumber: ''
};
export default class AccountLogin extends Component<IProps, IState> {
  state = initialState;

  static propTypes = {
    onAuthLogin: PropTypes.func
  };

  _dispatchAuthenticateUser = async () => {
    const { contactNumber } = this.state;
    const payload: IAccountLogin = {
      contactNumber,
      deviceDetails: {
        deviceId: getUniqueId(),
        deviceBrand: getBrand(),
        deviceManuacturer: await getManufacturer(),
        deviceMacAddress: await getMacAddress(),
        platform: Platform.OS,
        osArchitecture: String(await supportedAbis()),
        osName: getSystemName(),
        osVersion: getSystemVersion(),
        packageName: await getBundleId(),
        isDeviceRooted: await JailMonkey.isJailBroken(),
        isPinOrFingerprintSet: await isPinOrFingerprintSet(),
        isEmulator: await isEmulator()
      }
    };

    console.log('@@@MJ Payload: ', payload);

    if (this.props.onAuthLogin) {
      this.props.onAuthLogin(payload);
    }
  };

  _renderDeviceVerificationModal = () => {
    return <LottieView source={require('../../assets/anims/authenticate.json')} autoPlay loop />;
  };

  _renderOTPVerificationModal = () => {
    const { verificationOTPCode } = this.state;
    return (
      <BoxView>
        <BoxView padding="ysm">
          <InputTextView
            placeholder="Enter your OTP here"
            value={verificationOTPCode}
            onChange={verificationOTPCode => {
              this.setState({ verificationOTPCode });
            }}
            align="center"
            inputType="numeric"
            maxLength={6}
            size="xxlg"
            border="xs"
            background="white"
          />
        </BoxView>
        <BoxView direction="row" justify="spaceAround" padding="md">
          <ButtonView style="danger" size="xs" label={'RESEND OTP'} />
          <ButtonView style="danger" size="xs" label={'CLEAR FIELDS'} />
        </BoxView>
        <BoxView padding="ysm">
          <ButtonView style="info" size="xxlg" label={'CONFIRM OTP'} />
        </BoxView>
      </BoxView>
    );
  };

  _toggleVerifyingDeviceModal = () => {
    const { showVerifyingDeviceModal, contactNumber } = this.state;
    if (contactNumber === '' || contactNumber.length < 10) {
      Toast.show('Please enter a valid 10 digit contact number.');
      return;
    }
    this.setState({ showVerifyingDeviceModal: !showVerifyingDeviceModal }, () => {
      if (!showVerifyingDeviceModal) {
        this._dispatchAuthenticateUser();
        return;
      }
    });
  };

  _handleCancelInvitation = () => {};

  render() {
    const { showVerifyingDeviceModal, contactNumber } = this.state;
    const familyAnimation = require('../../assets/anims/family.json');

    return (
      <BoxView flex="one" background="primary">
        <ModalView
          position="cover"
          visible={showVerifyingDeviceModal}
          title="VERIFYING YOUR DEVICE"
          close={this._toggleVerifyingDeviceModal}
        >
          {this._renderDeviceVerificationModal()}
        </ModalView>

        <BoxView direction="row" padding="md" justify="spaceBetween" alignItems="center">
          <BoxView>
            <TextView size="xxlg" label={translate('GUEST_LOGIN_guestLogin')} weight="bold" />
            <TextView label={'realme SharedPaySa Guest Account '} weight="bold" />
          </BoxView>
          <Icon name="live-help" size={42} color={colorInfo} />
        </BoxView>
        <BoxView margin="md" elevation="sm" background="white" padding="md">
          <ButtonItemView>
            <BoxView padding="ymd">
              <BoxView padding="ysm" alignItems="center">
                <LottieView style={Styles.animationIcon} source={familyAnimation} autoPlay loop />
              </BoxView>
              <InputTextView
                placeholder="Type your phone number here"
                value={contactNumber}
                onChange={contactNumber => {
                  this.setState({ contactNumber });
                }}
                inputType="numeric"
                maxLength={10}
                size="lg"
                leftText="+91-"
                rightIcon="phone"
                elevation="sm"
                background="white"
              />
            </BoxView>
          </ButtonItemView>
          <BoxView padding="md" background="lightGrey" border="xs" round="xs">
            <TextView
              lines={2}
              label={`NOTE: Make sure the sim should be present in this phone for verification`}
              size="sm"
              weight="bold"
            />
          </BoxView>
        </BoxView>
        <BoxView margin="sm" direction="row" flex="one">
          <BoxView flex="one" margin="xs" elevation="sm">
            <ButtonView
              onClick={this._toggleVerifyingDeviceModal}
              style="info"
              size="xxlg"
              label={`SIGN-IN`}
            />
          </BoxView>
        </BoxView>
      </BoxView>
    );
  }
}
