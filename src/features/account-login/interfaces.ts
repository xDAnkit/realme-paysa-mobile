interface IDeviceDetails {
  deviceId: string;
  deviceBrand: string;
  deviceManuacturer: string;
  deviceMacAddress: string;
  platform: string;
  osName: string;
  osVersion: string;
  osArchitecture: string;
  packageName: string;
  isDeviceRooted: boolean;
  isPinOrFingerprintSet: boolean;
  isEmulator: boolean;
}

export interface IAccountLogin {
  contactNumber: string;
  deviceDetails: IDeviceDetails;
}

interface IAuthenticationSuccessfulResponse {
  appDeviceId: string;
  memberToken: string;
  refreshToken: string;
}

export interface IAuthSuccessResponse {
  items: IAuthenticationSuccessfulResponse;
  isFetching: boolean;
}

export interface IAuthRequest {
  type: string;
  payload: IAccountLogin;
}

export interface IProps {
  onAuthLogin?: (params: IAccountLogin) => void;
}

export interface IState {
  verificationOTPCode: string;
  verificationOTPToken: string;
  showVerifyingDeviceModal: boolean;
  contactNumber: string;
}

export interface AutheticateAccountReducer {
  items: any;
  isFetching: boolean;
}
export interface RootState {
  AutheticateAccountReducer: AutheticateAccountReducer;
}
