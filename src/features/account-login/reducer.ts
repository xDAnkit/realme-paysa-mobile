import { AUTHENTICATE_ACCOUNT, AUTHENTICATE_ACCOUNT_FAILED, AUTHENTICATE_ACCOUNT_SUCCEEDED } from './action';

const intialState = {
  isFetching: null,
  items: null
};
const AutheticateAccountReducer = (appDevice: any = intialState, action: any) => {
  switch (action.type) {
    case AUTHENTICATE_ACCOUNT:
      return {
        isFetching: true
      };
    case AUTHENTICATE_ACCOUNT_SUCCEEDED:
      return {
        isFetching: false,
        items: action.results
      };
    case AUTHENTICATE_ACCOUNT_FAILED:
      return {
        isFetching: false,
        items: {}
      };
    default:
      return appDevice;
  }
};

export default AutheticateAccountReducer;
