import { IAccountLogin, IAuthSuccessResponse } from './interfaces';

export const AUTHENTICATE_ACCOUNT = 'AUTHENTICATE_ACCOUNT';
export const AUTHENTICATE_ACCOUNT_SUCCEEDED = 'AUTHENTICATE_ACCOUNT_SUCCEEDED';
export const AUTHENTICATE_ACCOUNT_FAILED = 'AUTHENTICATE_ACCOUNT_FAILED';

export const authenticateAccountAction = (params: IAccountLogin) => {
  return { type: AUTHENTICATE_ACCOUNT, params };
};

export const authenticateAccountSuccessAction = (results: IAuthSuccessResponse) => {
  return { type: AUTHENTICATE_ACCOUNT_SUCCEEDED, results };
};

export const authenticateAccountFailedAction = () => {
  return { type: AUTHENTICATE_ACCOUNT_FAILED };
};
