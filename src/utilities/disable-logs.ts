/**
 * @format
 */
import { YellowBox } from 'react-native';

console.disableYellowBox = true;
YellowBox.ignoreWarnings([
  'Warning: isMounted(...) is deprecated',
  'Module RCTImageLoader',
]);

const disableConsole = (flag: boolean) => {
  if (flag) {
    console.log = function() {};
    console.warn = function() {};
    console.error = function() {};
  }
};

disableConsole(false);
