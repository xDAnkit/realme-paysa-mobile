export default {
  HOME_noteTitle: 'Note Title',
  HOME_pleaseTypeYourNote: 'Please type your note below',
  HOME_startTakingNotes: 'Start taking notes',
  HOME_save: 'Save',
  HOME_characters: 'chacters Eng',
  ABOUT_us: 'About Us',
  ABOUT_theApp: 'About the app',
  ABOUT_theCreators: 'About the Creators',
  ABOUT_theAppDesc: 'About the app',
  ABOUT_theCreatorsDesc: 'About the Creators',

  SPLASH_welcomeToRealmeSharedPaysa: 'Welcome to realme SharedPaySa',
  SPLASH_realmeSharedPaysaTagline: "Share with simplicity and we'll take care of security",
  SPLASH_myGuestUPIAccount: 'My Guest UPI Account',
  SPLASH_myUPIAccount: 'My Account',
  SPLASH_needHelp: 'Need Help?',

  INVITATION_createInvitation: 'Create Invitation',
  INVITATION_editInvitation: 'Edit Invitation',
  INVITATION_youCanCancelAnytime: 'NOTE: You can cancel this invitation at any moment',
  INVITATION_validTill: 'Valid Until',
  INVITATION_sendInvite: 'SEND INVITE',
  INVITATION_cancelInvite: 'CANCEL INVITE',
  INVITATION_selectedAccount: 'Selected Account',
  INVITATION_guestNameAccount: 'Guest Name & Account',
  INVITATION_elligibleAmount: 'Elligible Amount',
  INVITATION_amountValidity: 'Amount Validity',

  GUEST_LIST_myGuestAccounts: 'My Guest Accounts',

  GUEST_TRANSACTIONS_myguestTransactions: 'My Guest Transactions',
  GUEST_TRANSACTIONS_transactions: 'Transactions',

  GUEST_LOGIN_guestLogin: 'Guest Login',

  DASHBOARD_myPaysaOptions: 'My PaySa Options'
};
