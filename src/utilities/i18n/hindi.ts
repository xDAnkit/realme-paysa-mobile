export default {
  HOME_noteTitle: "Note Title",
  HOME_pleaseTypeYourNote: "Please type your note below",
  HOME_startTakingNotes: "Start taking notes",
  HOME_save: "Save",
  HOME_characters: "chacters Hin",
  ABOUT_us: "About Us",
  ABOUT_theApp: "About the app",
  ABOUT_theCreators: "About the Creators",
  ABOUT_theAppDesc: "About the app",
  ABOUT_theCreatorsDesc: "About the Creators",
  SPLASH_welcomeToRealmeSharedPaysa: "Realme SharedPaySa में आपका स्वागत है",
  SPLASH_realmeSharedPaysaTagline:
    "बिना किसी चिंता के साझा करें और हम सुरक्षा का ध्यान रखेंगे",

  SPLASH_myGuestUPIAccount: "मेरा अतिथि UPI खाता",
  SPLASH_myUPIAccount: "मेरा UPI खाता",
  SPLASH_needHelp: "मदद की ज़रूरत है?",

  INVITATION_createInvitation: "निमंत्रण बनाएँ",
  INVITATION_editInvitation: "निमंत्रण संपादित करें",
  INVITATION_youCanCancelAnytime:
    "नोट: आप किसी भी क्षण इस आमंत्रण को रद्द कर सकते हैं",
  INVITATION_validTill: "तब तक वैध",
  INVITATION_sendInvite: "आमंत्रण भेजो",
  INVITATION_cancelInvite: "आमंत्रण रद्द करें",
  INVITATION_selectedAccount: "चयनित खाता",
  INVITATION_guestNameAccount: "अतिथि का नाम और खाता",
  INVITATION_elligibleAmount: "पात्र राशि",
  INVITATION_amountValidity: "राशि की वैधता"
};
