import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { SUCCESS_STATUS, SUCCESS_WITH_CREATED_STATUS } from './http-codes';
import { BASE_URL } from '../url-properties';
function* GetRequest(url: string, authentication: boolean) {
  const config: AxiosRequestConfig = authentication
    ? yield GetHeadersWithAuthentication()
    : yield GetHeadersWithOutAuthentication();

  console.log('@@@@@`${BASE_URL}${url}`:', `${BASE_URL}${url}`);
  const results: AxiosResponse = yield axios.get(`${BASE_URL}${url}`, config);
  const { status, data } = results;
  return status === SUCCESS_STATUS ? data : {};
}

function* PostRequest(url: string, params: any, authentication: boolean) {
  const config: AxiosRequestConfig = authentication
    ? yield GetHeadersWithAuthentication()
    : yield GetHeadersWithOutAuthentication();
  const results: AxiosResponse = yield axios.post(`${BASE_URL}${url}`, params, config);
  const { status, data } = results;
  return status === SUCCESS_WITH_CREATED_STATUS || status === SUCCESS_STATUS ? data : {};
}

function* PutRequest(url: string, params: any) {
  const config: AxiosRequestConfig = yield GetHeadersWithAuthentication();
  const results: AxiosResponse = yield axios.put(`${BASE_URL}${url}`, params, config);
  const { status, data } = results;
  return status === SUCCESS_STATUS ? data : {};
}

function* DeleteRequest(url: string) {
  const config: AxiosRequestConfig = yield GetHeadersWithAuthentication();
  const axiosConfig = {
    ...config
  };
  const response = yield axios.delete(`${BASE_URL}${url}`, axiosConfig);
  const resolvedResponse = response.status === SUCCESS_STATUS ? response.data : {};
  return resolvedResponse;
}

function* GetHeadersWithAuthentication() {
  const userToken = 'TOKEN';
  const config: AxiosRequestConfig = yield {
    headers: {
      Accept: 'application/json',
      Authorization: userToken
    }
  };
  return config;
}

function* GetHeadersWithOutAuthentication() {
  const config: AxiosRequestConfig = yield {
    headers: {
      Accept: 'application/json'
    }
  };
  return config;
}

export { GetRequest, PostRequest, PutRequest, DeleteRequest };
