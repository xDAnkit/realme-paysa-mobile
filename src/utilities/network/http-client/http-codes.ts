export const SUCCESS_STATUS = 200;
export const SUCCESS_WITH_CREATED_STATUS = 201;
export const UNAUTHORIZED = 401;
export const BAD_REQUEST = 400;
export const SERVER_ERROR = 500;
