export const BASE_URL = 'http://192.168.1.28:5000/';

// * Auth URL
export const AUTH_ACCOUNT_URL = 'authentication/validate-contact';

// * Create Invitation
export const INVITATION_URL = 'invitation';

// * Transaction Account
export const TRANSACTION_ACCOUNT_URL = 'transaction-account';
