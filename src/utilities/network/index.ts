export {
  GetRequest as GET,
  PostRequest as POST,
  PutRequest as PUT,
  DeleteRequest as Delete
} from './http-client';

import { IHTTPResponse } from './http-client/interface';
export type IHTTPDataResponse = IHTTPResponse;

export {
  SERVER_ERROR,
  SUCCESS_STATUS,
  SUCCESS_WITH_CREATED_STATUS,
  BAD_REQUEST,
  UNAUTHORIZED
} from './http-client/http-codes';

export {
  INVITATION_URL as INVITATION_API,
  BASE_URL as BASE_API,
  AUTH_ACCOUNT_URL as AUTH_ACCOUNT_API
} from './url-properties';
