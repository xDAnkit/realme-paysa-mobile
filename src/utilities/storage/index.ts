import * as SecureStorage from './secure';
import * as RegularStorage from './regular';

export const GetItem = async (key: string, secure: boolean = false) => {
  if (!secure) {
    const results = await RegularStorage.GetItem(key);
    return results;
  }
  const results = await SecureStorage.GetItem(key);
  return results;
};

export const GetMultipleItems = async (keys: any) => {
  const results = await RegularStorage.GetMultipleItem(keys);
  return results;
};

export const SetItem = async (
  key: string,
  value: string,
  secure: boolean = false,
) => {
  if (!secure) {
    await RegularStorage.SetItem(key, value);
    return;
  }
  await SecureStorage.SetItem(key, value);
  return;
};

export const ClearItem = async (key: string, secure: boolean = false) => {
  if (!secure) {
    await RegularStorage.ClearItem(key);
    return;
  }
  await SecureStorage.ClearItem(key);
  return;
};

export const ClearAllItem = async () => {
  await RegularStorage.ClearAllItems();
  return;
};

export const GetAllKeys = async (key: string, secure: boolean = false) => {
  if (!secure) {
    await RegularStorage.GetAllItems();
    return;
  }
  await SecureStorage.GetAllItems();
  return;
};
