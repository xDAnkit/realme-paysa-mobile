import SecureStorage, {
  ACCESS_CONTROL,
  ACCESSIBLE,
  AUTHENTICATION_TYPE,
} from 'react-native-secure-storage';

const SECURE_STORAGE_CONFIG = {
  accessControl: ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
  accessible: ACCESSIBLE.WHEN_UNLOCKED,
  authenticationPrompt:
    'Please add Pin/Pattern or any biometric lock before using this app',
  service: 'all',
  authenticateType: AUTHENTICATION_TYPE.BIOMETRICS,
};

export const GetItem = async (key: string) => {
  const results = await SecureStorage.getItem(key, SECURE_STORAGE_CONFIG);
  return results ? results : null;
};

export const SetItem = async (key: string, value: string) => {
  await SecureStorage.setItem(key, value, SECURE_STORAGE_CONFIG);
};

export const ClearItem = async (key: string) => {
  const results = await SecureStorage.getItem(key, SECURE_STORAGE_CONFIG);
  return results ? results : null;
};

export const GetAllItems = async () => {
  const results = await SecureStorage.GetAllKeys(SECURE_STORAGE_CONFIG);
  return results ? results : null;
};
