import AsyncStorage from '@react-native-community/async-storage';

export const GetItem = async (key: string) => {
  let result = null;
  try {
    result = await AsyncStorage.getItem(key);
  } catch (error) {}
  return result;
};

export const GetMultipleItem = async (keys: any) => {
  let result = null;
  try {
    result = await AsyncStorage.multiGet(keys);
  } catch (error) {}
  return result;
};

export const GetAllItems = async () => {
  let result = null;
  try {
    result = await AsyncStorage.getAllKeys();
  } catch (error) {}
  return result;
};

export const SetItem = async (key: string, value: string) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {}
};

export const ClearItem = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {}
};

export const ClearAllItems = async () => {
  try {
    await AsyncStorage.clear();
  } catch (error) {}
};
