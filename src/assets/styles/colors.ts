// Light Style
export const COLOR_PRIMARY = '#12193D';
export const COLOR_PRIMARY_DARK = '#7f0000';
export const COLOR_ACCENT = '#582697';
export const COLOR_ACCENT_DARK = '#E7B935';
export const COLOR_TEXT_PRIMARY = '#34495e';
export const COLOR_TEXT_SECONDARY = '#8babcc';
export const COLOR_DISABLED = '#eee';
export const COLOR_BACKGROUND = '#F6F9FE';
export const COLOR_WHITE = '#fff';
export const COLOR_BLACK = '#000';
export const COLOR_TRANSPARENT = 'transparent';
export const COLOR_DIVIDER = '#ccc';
export const COLOR_DIVIDER_DARK = '#ccc';
export const COLOR_HEADER = '#eee';
export const COLOR_LIGHT_GREY = '#f5f5f5';
export const COLOR_DANGER = '#e15f41';
export const COLOR_SUCCESS = '#16a085';
export const COLOR_INFO = '#45aaf2';

// Dark Style
