export const FONT_LIGHT = "BalooBhai2-Light";
export const FONT_REGULAR = "BalooBhai2-Regular";
export const FONT_MEDIUM = "BalooBhai2-Medium";
export const FONT_SEMIBOLD = "BalooBhai2-SemiBold";
export const FONT_BOLD = "BalooBhai2-Bold";
