// Style Config

// Text Style
export const EXTRA_SMALL_TEXT_SIZE = 11;
export const SMALL_TEXT_SIZE = 12;
export const NORMAL_TEXT_SIZE = 14;
export const MEDIUM_TEXT_SIZE = 16;
export const LARGE_TEXT_SIZE = 18;
export const EXTRA_LARGE_TEXT_SIZE = 22;
export const DOUBLE_EXTRA_LARGE_TEXT_SIZE = 28;

// Icon style
export const EXTRA_SMALL_ICON_SIZE = 11;
export const SMALL_ICON_SIZE = 12;
export const NORMAL_ICON_SIZE = 14;
export const MEDIUM_ICON_SIZE = 16;
export const LARGE_ICON_SIZE = 18;
export const EXTRA_LARGE_ICON_SIZE = 22;
export const DOUBLE_EXTRA_LARGE_ICON_SIZE = 28;
