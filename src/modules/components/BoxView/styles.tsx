import { StyleSheet, Platform } from 'react-native';
import {
  colorTextSecondary,
  colorDisabled,
  colorWhite,
  colorDanger,
  colorSuccess,
  colorTransparent,
  colorLightGrey,
  colorInfo,
  colorAccent,
  colorPrimary,
  colorBackground
} from '../../../assets/styles';

export const styles: any = {
  // Flex

  flex: {
    one: {
      flex: 1
    },
    two: {
      flex: 2
    },
    three: {
      flex: 3
    },
    four: {
      flex: 4
    },
    five: {
      flex: 5
    },
    six: {
      flex: 6
    },
    seven: {
      flex: 7
    },
    eight: {
      flex: 8
    },
    nine: {
      flex: 9
    }
  },

  // Content Direction
  row: {
    flexDirection: 'row'
  },
  column: {
    flexDirection: 'column'
  },

  // margin
  m6: {
    margin: 6
  },
  m12: {
    margin: 12
  },

  padding: {
    xs: {
      padding: 2
    },
    yxs: {
      paddingTop: 4,
      paddingBottom: 4
    },
    xSMyXS: {
      paddingLeft: 6,
      paddingRight: 6,
      paddingTop: 12,
      paddingBottom: 12
    },
    sm: {
      padding: 6
    },
    xsm: {
      paddingLeft: 6,
      paddingRight: 6
    },
    xxs: {
      paddingLeft: 4,
      paddingRight: 4
    },
    md: {
      padding: 12
    },
    lg: {
      padding: 16
    },
    xlg: {
      padding: 18
    },
    xmd: {
      paddingTop: 12,
      paddingBottom: 12
    },
    ysm: {
      paddingTop: 6,
      paddingBottom: 6
    },
    ymd: {
      paddingTop: 12,
      paddingBottom: 12
    }
  },
  margin: {
    lrbsm: {
      marginLeft: 6,
      marginRight: 6,
      marginBottom: 6
    },
    yxs: {
      marginTop: 4,
      marginBottom: 4
    },
    xs: {
      margin: 2
    },
    xss: {
      margin: 2
    },
    xsTop: {
      marginTop: 2
    },
    sm: {
      margin: 6
    },
    xsm: {
      marginLeft: 6,
      marginRight: 6
    },
    xxs: {
      marginLeft: 4,
      marginRight: 4
    },
    smTop: {
      marginTop: 6
    },
    ysm: {
      marginTop: 2,
      marginBottom: 2
    },
    ymd: {
      marginTop: 12,
      marginBottom: 12
    },
    xmd: {
      marginLeft: 12,
      marginRight: 12
    },
    md: {
      margin: 12
    }
  },

  // Align items

  alignContent: {
    center: {
      alignContent: 'center'
    },
    flexEnd: {
      alignContent: 'flex-end'
    },
    flexStart: {
      alignContent: 'flex-start'
    },
    spaceAround: {
      alignContent: 'space-around'
    },
    spaceBetween: {
      alignContent: 'space-between'
    },
    stretch: {
      alignContent: 'stretch'
    }
  },

  alignItems: {
    center: {
      alignItems: 'center'
    },
    flexEnd: {
      alignItems: 'flex-end'
    },
    flexStart: {
      alignItems: 'flex-start'
    },
    spaceAround: {
      alignItems: 'space-around'
    },
    spaceBetween: {
      alignItems: 'space-between'
    },
    stretch: {
      alignItems: 'stretch'
    }
  },

  justify: {
    center: {
      justifyContent: 'center'
    },
    flexEnd: {
      justifyContent: 'flex-end'
    },
    flexStart: {
      justifyContent: 'flex-start'
    },
    spaceAround: {
      justifyContent: 'space-around'
    },
    spaceBetween: {
      justifyContent: 'space-between'
    },
    spaceEvenly: {
      justifyContent: 'space-evenly'
    }
  },

  // Align self

  alignSelf: {
    center: {
      alignSelf: 'center'
    },
    flexEnd: {
      alignSelf: 'flex-end'
    },
    flexStart: {
      alignSelf: 'flex-start'
    },
    auto: {
      alignSelf: 'auto'
    },
    baseline: {
      alignSelf: 'baseline'
    },
    stretch: {
      alignSelf: 'stretch'
    }
  },

  border: {
    xs: {
      borderWidth: 0.6,
      borderColor: colorDisabled
    },
    xxs: {
      borderRightWidth: 0.6,
      borderRightColor: colorDisabled
    },
    sm: {
      borderWidth: 1,
      borderColor: colorDisabled
    },
    md: {
      borderWidth: 1.3,
      borderColor: colorDisabled
    },
    topXs: {
      borderTopWidth: 1,
      borderTopColor: colorDisabled
    },
    bottomXs: {
      borderBottomWidth: 1,
      borderBottomColor: colorDisabled
    },
    bottom: {
      borderBottomWidth: 1,
      borderBottomColor: colorDisabled
    },
    top: {
      borderTopWidth: 1,
      borderTopColor: colorDisabled
    },
    right: {
      borderRightWidth: 1,
      borderRightColor: colorDisabled
    }
  },

  // Background
  background: {
    primary: {
      backgroundColor: colorBackground
    },
    white: {
      backgroundColor: colorWhite
    },
    transparent: {
      backgroundColor: colorTransparent
    },
    grey: {
      backgroundColor: colorDisabled
    },
    lightGrey: {
      backgroundColor: colorLightGrey
    },
    danger: {
      backgroundColor: colorDanger
    },
    success: {
      backgroundColor: colorSuccess
    },
    warning: {
      backgroundColor: colorAccent
    },
    info: {
      backgroundColor: colorInfo
    }
  },

  // Position
  position: {
    bottom: {
      flex: 1,
      padding: 0,
      margin: 0,
      left: 0,
      right: 0,
      position: 'absolute',
      bottom: 0
    },
    top: {
      flex: 1,
      padding: 0,
      margin: 0,
      left: 0,
      right: 0,
      top: 0,
      position: 'absolute'
    }
  },
  elevation: {
    sm: {
      shadowOffset: {
        width: 0,
        height: 0
      },

      shadowOpacity: Platform.OS === 'android' ? 0.5 : 0.2,
      shadowRadius: Platform.OS === 'android' ? 2 : 1,
      elevation: Platform.OS === 'android' ? 2 : 1
    },
    md: {
      shadowOffset: {
        width: 2,
        height: 2
      },
      shadowOpacity: 1,
      shadowRadius: 1,
      elevation: 20
    }
  },
  round: {
    xs: {
      borderRadius: 6
    },
    sm: {
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 200,
      width: 38,
      height: 38
    },
    md: {
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 200,
      width: 42,
      height: 42
    },
    lg: {
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 200,
      width: 52,
      height: 52
    }
  },
  curve: {
    sm: {
      borderRadius: 12
    }
  },
  height: {
    '400': {
      height: 300
    }
  },
  radius: {
    xs: {
      borderRadius: 40
    },
    sm: {
      borderRadius: 6
    }
  }
};
