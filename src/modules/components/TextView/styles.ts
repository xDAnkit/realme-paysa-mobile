import {
  colorTextPrimary,
  colorTextSecondary,
  colorAccentDark,
  fontLight,
  fontRegular,
  fontMedium,
  fontSemiBold,
  fontBold,
  colorWhite
} from "../../../assets/styles";

export const styles: any = {
  // Weight Styling
  light: {
    fontFamily: fontLight
  },
  regular: {
    fontFamily: fontRegular
  },
  medium: {
    fontFamily: fontMedium
  },
  semiBold: {
    fontFamily: fontSemiBold
  },
  bold: {
    fontFamily: fontBold
  },
  none: {},
  noneCancelled: {
    textDecorationLine: "line-through"
  },
  noneBold: {
    fontWeight: "bold"
  },
  // Text Color
  primary: {
    color: colorTextPrimary
  },
  secondary: {
    color: colorTextSecondary
  },
  accent: {
    color: colorAccentDark
  },
  white: {
    color: colorWhite
  },

  // Text sizes
  xs: {
    fontSize: 11
  },
  sm: {
    fontSize: 12
  },
  md: {
    fontSize: 13
  },
  lg: {
    fontSize: 16
  },
  xlg: {
    fontSize: 23
  },
  xxlg: {
    fontSize: 28
  },
  xxxlg: {
    fontSize: 48
  },

  // Text Alignment
  center: {
    textAlign: "center"
  },
  left: {
    textAlign: "left"
  },
  right: {
    textAlign: "right"
  }
};
