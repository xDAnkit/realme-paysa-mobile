/**
 * @format
 */

import React from 'react';
import { TextInput } from 'react-native';
import PropTypes from 'prop-types';
import { styles } from './styles';
import {
  colorWhite,
  colorSuccess,
  colorInfo,
  colorTransparent,
  background,
  colorTextSecondary,
  colorDisabled,
  colorPrimary,
  colorAccent,
  colorTextPrimary
} from '../../../assets/styles';
import BoxView from '../BoxView';
import TextView from '../TextView';
import Icon from 'react-native-vector-icons/MaterialIcons';

/**
 * @description {This InputTextView can be used for taking input from a user. It provides following things mentioned below}
 * Normal Input Text
 * Disabled Input Text
 * Input Text with Left icon
 * Input Text with Right Icon
 * Input Text with Left Text
 */

const _renderTextInput = (props: IInputTextViewProps) => {
  const {
    value,
    align = '',
    size = '',
    placeholder,
    maxLength,
    suggestions,
    editable,
    focus,
    inputType,
    multiline,
    lines,
    onChange
  } = props;

  const focusElem = focus === 'left' ? { start: 0, end: 0 } : undefined;

  return (
    <TextInput
      selection={focusElem}
      editable={editable}
      multiline={multiline}
      numberOfLines={lines}
      maxLength={maxLength}
      style={[styles.inputText, styles['align'][align], styles['size'][size]]}
      keyboardType={inputType}
      returnKeyType="done"
      value={value}
      placeholderTextColor={colorDisabled}
      autoCorrect={suggestions}
      underlineColorAndroid={colorTransparent}
      placeholder={placeholder}
      onChangeText={text => onChange(text)}
    />
  );
};

const InputTextView: React.FC<IInputTextViewProps> = props => {
  const {
    size,
    leftIcon = '',
    leftText = '',
    rightIcon = '',
    rightText = '',
    background = '',
    elevation = '',
    round = '',
    border = ''
  } = props;

  // Case 1   - Having Right or left text
  if (leftText !== '' || rightText !== '') {
    const leftTextView =
      leftText !== '' ? (
        <BoxView padding="xsm">
          <TextView label={leftText} weight="bold" size={size} />
        </BoxView>
      ) : null;
    const rightTextView =
      rightText !== '' ? (
        <BoxView padding="xsm">
          <TextView label={rightText} size={size} />
        </BoxView>
      ) : null;
    return (
      <BoxView
        border={border}
        round={round}
        direction="row"
        alignItems="center"
        background={background}
        elevation={elevation}
      >
        {leftTextView}
        {_renderTextInput(props)}
        {rightTextView}
      </BoxView>
    );
  }

  // Case 2   - Having Righr or left icon
  if (leftIcon !== '' || rightIcon !== '') {
    const leftIconView =
      leftIcon !== '' ? (
        <BoxView padding="xsm">
          <Icon name={leftIcon} size={24} color={colorTextPrimary} />
        </BoxView>
      ) : null;
    const rightIconView =
      rightIcon !== '' ? (
        <BoxView padding="xsm">
          <Icon name={rightIcon} size={24} color={colorTextPrimary} />
        </BoxView>
      ) : null;

    return (
      <BoxView
        border={border}
        round={round}
        direction="row"
        alignItems="center"
        background={background}
        elevation={elevation}
      >
        {leftIconView}
        {_renderTextInput(props)}
        {rightIconView}
      </BoxView>
    );
  }

  // Case 3   - Regular Input Text
  return (
    <BoxView
      border={border}
      round={round}
      direction="row"
      alignItems="center"
      background={background}
      elevation={elevation}
    >
      {_renderTextInput(props)}
    </BoxView>
  );
};

interface IInputTextViewProps {
  value: string;
  maxLength?: number;
  align?: string;
  size?: string;
  border?: string;
  placeholder?: string;
  suggestions?: boolean;
  multiline?: boolean;
  lines?: number;
  editable?: boolean;
  inputType?: any;
  background?: string;
  leftIcon?: string;
  leftText?: string;
  rightIcon?: string;
  rightText?: string;
  round?: string;
  elevation?: string;
  focus?: string;
  onChange: (text: any) => void;
}

InputTextView.defaultProps = {
  value: '',
  maxLength: 255,
  align: '',
  size: '',
  border: '',
  placeholder: '',
  suggestions: true,
  multiline: false,
  lines: 1,
  background: 'white',
  editable: true,
  inputType: 'default',
  leftIcon: undefined,
  leftText: undefined,
  rightIcon: undefined,
  rightText: undefined,
  round: '',
  elevation: '',
  focus: '',
  onChange: undefined
};

InputTextView.propTypes = {
  value: PropTypes.string.isRequired,
  align: PropTypes.string,
  size: PropTypes.string,
  border: PropTypes.string,
  maxLength: PropTypes.number.isRequired,
  placeholder: PropTypes.string,
  suggestions: PropTypes.bool,
  multiline: PropTypes.bool,
  lines: PropTypes.number,
  background: PropTypes.oneOf(['default', 'white', 'grey']),
  editable: PropTypes.bool,
  inputType: PropTypes.string,
  leftIcon: PropTypes.string,
  leftText: PropTypes.string,
  rightIcon: PropTypes.string,
  rightText: PropTypes.string,
  round: PropTypes.string,
  elevation: PropTypes.string,
  focus: PropTypes.string,
  onChange: PropTypes.func.isRequired || null
};

export default InputTextView;
