/**
 * @format
 */

import React from "react";
import {
  TouchableOpacity,
  Platform,
  TouchableNativeFeedback,
  TouchableHighlight
} from "react-native";
import PropTypes from "prop-types";

const ButtonItemView: React.FC<ButtonItemProps> = props => {
  const { onClick } = props;
  return Platform.OS === "android" ? (
    <TouchableNativeFeedback onPress={onClick}>
      {props.children}
    </TouchableNativeFeedback>
  ) : (
    <TouchableOpacity activeOpacity={0.7} onPress={onClick}>
      {props.children}
    </TouchableOpacity>
  );
};

interface ButtonItemProps {
  onClick?: () => void;
}

ButtonItemView.defaultProps = {
  onClick: undefined
};

ButtonItemView.propTypes = {
  onClick: PropTypes.func || null
};

export default ButtonItemView;
