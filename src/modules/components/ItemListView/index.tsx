/**
 * @format
 */

import React from 'react';
import { FlatList } from 'react-native';
import ContentLoader, { Rect, Circle } from 'react-content-loader/native';
import PropTypes from 'prop-types';

const ButtonView: React.FC<ItemListViewProps> = props => {
  const { records, component, click_1, click_2, click_3, click_4, click_5 } = props;
  const { itemList } = records;
  console.log('@@@@@Records: ', records);

  const ContentComponent = component;
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      data={itemList}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item, index }) => {
        return (
          <ContentComponent
            click_1={click_1}
            click_2={click_2}
            click_3={click_3}
            click_4={click_4}
            click_5={click_5}
            item={item}
            index={index}
          />
        );
      }}
    />
  );
};

interface IRecords {
  totalRecords: number | null | undefined;
  itemList: any | null;
  hasRecords: boolean | null;
}

interface ItemListViewProps {
  records: IRecords;
  component: any;
  retryClick?: () => void;
  click_1?: (params: any) => void;
  click_2?: (params: any) => void;
  click_3?: (params: any) => void;
  click_4?: (params: any) => void;
  click_5?: (params: any) => void;
}

ButtonView.defaultProps = {
  records: {
    totalRecords: null,
    itemList: null,
    hasRecords: null
  },
  retryClick: undefined,
  click_1: undefined,
  click_2: undefined,
  click_3: undefined,
  click_4: undefined,
  click_5: undefined
};

ButtonView.propTypes = {
  records: PropTypes.any,
  retryClick: PropTypes.func || null,
  click_1: PropTypes.func || undefined,
  click_2: PropTypes.func || null,
  click_3: PropTypes.func || null,
  click_4: PropTypes.func || null,
  click_5: PropTypes.func || null
};

export default ButtonView;
