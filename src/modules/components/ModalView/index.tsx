/**
 * Author: Ankit Jain
 * Organization: Mahak Labs
 * Project Name: InOffice
 * Date: 10-Jan-2020
 */

import * as React from "react";
import { TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/MaterialIcons";
import TextView from "../TextView";
import { styles } from "./styles";
import BoxView from "../BoxView";

const ModalView: React.FC<ModalProps> = props => {
  const { visible, title, close, hide, position = "bottom" } = props;
  return (
    <Modal
      useNativeDriver={true}
      hideModalContentWhileAnimating
      backdropTransitionOutTiming={0}
      onModalHide={hide}
      onBackButtonPress={close}
      avoidKeyboard={true}
      onBackdropPress={close}
      style={styles[position]}
      isVisible={visible}
    >
      <BoxView
        elevation="md"
        background="primary"
        flex={position === "cover" ? "one" : ""}
      >
        <BoxView
          direction="row"
          justify="spaceBetween"
          alignItems="center"
          padding="md"
          border="bottom"
        >
          <TextView label={title} weight="bold" size="lg" />
          <TouchableOpacity
            testID="btnCloseModal"
            onPress={close}
            activeOpacity={0.7}
          >
            <Icon name="cancel" size={34} color="#000" />
          </TouchableOpacity>
        </BoxView>
        <BoxView padding="md" flex={position === "cover" ? "nine" : ""}>
          {props.children}
        </BoxView>
      </BoxView>
    </Modal>
  );
};

interface ModalProps {
  visible: boolean;
  title: string;
  position?: string;
  close: () => void;
  hide?: () => void;
}

ModalView.defaultProps = {
  visible: false,
  title: "",
  position: "bottom",
  close: undefined,
  hide: undefined
};

ModalView.propTypes = {
  visible: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  position: PropTypes.string,
  close: PropTypes.func.isRequired || null,
  hide: PropTypes.func || null
};

export default ModalView;
