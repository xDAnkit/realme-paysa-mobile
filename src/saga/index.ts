//Saga effects
import { all, fork } from 'redux-saga/effects';
import { watchFetchInvitedGuestList } from '../features/guest-list/saga';
import { watchCreateInvitation } from '../features/invite-guest/saga';
import { watchFetchTransactionAccountDetails } from '../features/guest-details/saga';

export default function* rootSaga() {
  yield all([
    fork(watchFetchInvitedGuestList),
    fork(watchCreateInvitation),
    fork(watchFetchTransactionAccountDetails)
  ]);
}
