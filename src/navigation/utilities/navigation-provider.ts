import { navigate, goBack } from './navigator';

export const NavigateToHome = () => {
  console.log('@@@@Req came here');
  navigate('home');
};

export const NavigateToSplash = () => {
  goBack('splash');
};

export const NavigateToCreateInvitation = (params?: any) => {
  navigate('GuestInvitation', params);
};

export const NavigateToSentInvitationsList = () => {
  navigate('GuestList');
};

export const NavigateToMakeTransactionView = () => {
  navigate('MakeTransaction');
};

export const NavigateToGuestDetailsView = (params: any) => {
  navigate('GuestDetails', params);
};
