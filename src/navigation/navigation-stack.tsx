import * as React from 'react';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import translate from '../../src/utilities/i18n';
import SplashScreen from '../features/splash';
import HomeScreen from '../features/home';
import InviteGuestScreen from '../features/invite-guest';
import GuestListScreen from '../features/guest-list';
import GuestTransactionsScreen from '../features/guest-transactions';
import AccountLogin from '../features/account-login';
import UPIPinScreen from '../features/upi-pin';
import MakeTransactionScreen from '../features/make-transaction';
import GuestDetailsScreen from '../features/guest-details';
import DashboardScreen from '../features/dashboard';
import { setNavigator } from './utilities/navigator';
import { colorBackground, colorTextPrimary, fontMedium, fontBold } from '../assets/styles';
import { Platform, StatusBar } from 'react-native';

const Stack = createStackNavigator();

function RootNavigation() {
  const ref = React.useRef(null);
  setNavigator(ref);
  return (
    <NavigationContainer ref={ref}>
      <StatusBar backgroundColor="white" barStyle="dark-content" />

      <Stack.Navigator>
        <Stack.Screen
          name="Dashboard"
          component={DashboardScreen}
          options={{
            title: '',
            headerStyle: {
              backgroundColor: colorBackground,
              elevation: 0,
              shadowOpacity: 0
            },
            headerTitleStyle: {
              color: colorTextPrimary,
              fontFamily: fontBold,
              fontSize: 14
            },
            headerBackTitleStyle: {
              fontWeight: '900',
              fontSize: 14
            }
          }}
        />
        <Stack.Screen
          name="GuestDetails"
          component={GuestDetailsScreen}
          options={{
            title: '',
            headerStyle: {
              backgroundColor: colorBackground,
              elevation: 0,
              shadowOpacity: 0
            },
            headerTitleStyle: {
              color: colorTextPrimary,
              fontFamily: fontBold,
              fontSize: 14
            },
            headerBackTitleStyle: {
              fontWeight: '900',
              fontSize: 14
            }
          }}
        />
        <Stack.Screen
          name="MakeTransaction"
          component={MakeTransactionScreen}
          options={{
            title: '',
            headerStyle: {
              backgroundColor: colorBackground,
              elevation: Platform.OS === 'android' ? 1 : 0,
              shadowOpacity: Platform.OS === 'android' ? 1 : 0
            },
            headerTitleStyle: {
              color: colorTextPrimary,
              fontFamily: fontBold,
              fontSize: 14
            },
            headerBackTitleStyle: {
              fontWeight: '900',
              fontSize: 14
            }
          }}
        />
        <Stack.Screen
          name="GuestTransactions"
          component={GuestTransactionsScreen}
          options={{
            title: translate('GUEST_TRANSACTIONS_myguestTransactions'),
            headerStyle: {
              backgroundColor: colorBackground,
              elevation: Platform.OS === 'android' ? 1 : 1,
              shadowOpacity: Platform.OS === 'android' ? 1 : 1
            },
            headerTitleStyle: {
              color: colorTextPrimary,
              fontFamily: fontBold,
              fontSize: 14
            },
            headerBackTitleStyle: {
              fontWeight: '900',
              fontSize: 14
            }
          }}
        />
        <Stack.Screen
          name="GuestInvitation"
          component={InviteGuestScreen}
          options={{
            title: '',
            headerStyle: {
              backgroundColor: colorBackground,
              elevation: Platform.OS === 'android' ? 1 : 0,
              shadowOpacity: Platform.OS === 'android' ? 1 : 0
            },
            headerTitleStyle: {
              color: colorTextPrimary,
              fontFamily: fontBold,
              fontSize: 14
            },
            headerBackTitleStyle: {
              fontWeight: '900',
              fontSize: 14
            }
          }}
        />
        <Stack.Screen
          name="GuestList"
          component={GuestListScreen}
          options={{
            title: '',
            headerStyle: {
              backgroundColor: colorBackground,
              elevation: Platform.OS === 'android' ? 1 : 0,
              shadowOpacity: Platform.OS === 'android' ? 1 : 0
            },
            headerTitleStyle: {
              color: colorTextPrimary,
              fontFamily: fontBold,
              fontSize: 14
            },
            headerBackTitleStyle: {
              fontWeight: '900',
              fontSize: 14
            }
          }}
        />
        <Stack.Screen name="splash" component={SplashScreen} />
        <Stack.Screen name="home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default RootNavigation;
