import { combineReducers } from 'redux';
import AutheticateAccountReducer from '../features/account-login/reducer';
import FetchInvitedGuestReducer from '../features/guest-list/reducer';
import CreateInvitationReducer from '../features/invite-guest/reducer';
import FetchTransactionAccountDetailsReducer from '../features/guest-details/reducer';
const AppReducer = combineReducers({
  AutheticateAccountReducer,
  FetchInvitedGuestReducer,
  CreateInvitationReducer,
  FetchTransactionAccountDetailsReducer
});
export default AppReducer;
